const path = require('path');
const WebpackUserscript = require('webpack-userscript')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const dev = process.env.NODE_ENV === 'development'
const package_json = require('./package.json');

module.exports = {
  // mode: 'development',
  // devServer: {
  //   contentBase: path.join(__dirname, 'dist'),
  //   compress: true,
  //   port: 9000
  // },
  entry: {
      'paperclip-helper': './src/index.js'
  },
  output: {
    filename: '[name]-' + package_json.version + '.user.js',
    path: path.resolve(__dirname, 'dist'),
  },
  optimization: {
    minimize: false
  },
  plugins: [
    new CleanWebpackPlugin(),
    new WebpackUserscript({
    	headers: {
        	version:     dev ? `[version]-build.[buildNo]` : `[version]`,
        	description: 'Try to take over the world!  Script from github (below) tweaked and enhanced by Jason, and also converted to TamperMonkey script based off of https://github.com/takayatodoroki/paperclipsHelper',
        	author:      'Jason & https://gist.github.com/stloewen/a8db68846489753b076527ae8da4ab94',
			match:       'https://www.decisionproblem.com/paperclips/index2.html',
			grant:       ['GM_xmlhttpRequest','unsafeWindow'],
			connect:     ['192.168.1.7','*'],

      }
    })
  ]
};

