/* TamperMonkey globals */
/* global GM_xmlhttpRequest, */
       
export class ElasticJsonAppender {
    constructor (filterFunction) {
        this.filterFunction = filterFunction
        this.index = "paperclip-log"
        this.elasticUrl = "http://192.168.1.7:32200/"
    }

    log(obj) {
        if (! this.filterFunction || this.filterFunction() ) {
            GM_xmlhttpRequest({
                method: "POST",
                url: this.elasticUrl + this.index + "/_doc/",
                headers: {  "Content-Type": "application/json"},
                data: JSON.stringify(obj),
                onload: function(response) {
                    if (response.status != 201) {
                        console.log('Unexpected response from elastic: ' + response.responseText)
                        console.log(obj, response)
                    }
                },
                onerror: function(r) {
                    console.log('error POSTing data', r)
                }
            });
        }
    }
}
