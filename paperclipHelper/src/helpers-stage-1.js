import {btnActive,divVisible,optionTicked,getText,getStage,getNumber,projectVisible,activateAvailableProjects,getProject } from './helper-functions.js'
import {eventLogger} from './json-event-logger.js'

const MAX_SALE_PRICE=1.00



/* Paperclip globals */
/* global buyWire,clipClick,makeClipper,makeMegaClipper,addProc,addMem,lowerPrice,raisePrice,buyAds,investUpgrade,investDeposit,investWithdraw */

// Phase 1
///////////////////////////
//  automake paperclips  //
///////////////////////////
window.setInterval(function() {
    if (
        optionTicked("autoMakePaperclips")
        &&
        (
            ! divVisible('wireBuyerDiv')
            ||
            (
                divVisible('wireBuyerDiv')
                &&
                getText('wireBuyerStatus') != 'ON'
            )
        )
        &&
        btnActive('btnMakePaperclip')) {
        clipClick(10000000);
    }
}, 100);

////////////////////
//  autobuy wire  //
////////////////////
window.setInterval(function() {
    if (optionTicked("autoBuyWire")) {
        if (getText('wire') == 0) {
            buyWire()
        }
    }
}, 100);


const reasonsToWithdraw=[
    {projectId:'projectButton37',  requiredCash:3000000},     // Hostile Takeover - $1 million - don't withdraw until you have $3 million so you keep momentum
    {projectId:'projectButton38',  requiredCash:30000000},    // Full Monopoly - $10 mil. - don't withdraw until $30 mill for momentum
    {projectId:'projectButton40',  requiredCash:1000000000},  // Token of Goodwill           | The total value of ALL depends on how much trust you need.  
    {projectId:'projectButton40b', requiredCash:1000000000},  // Another token of Goodwills  | I think I've seen it hit $1 Billion 
]

const clipMilestones = [
    [10,12],
    [100,110],
    [1000,1100],
    [10000,110000],
    [50000,55000],
    [100000,110000],
]

export class StageOnePlayer {
    constructor () {
        this.doneWithStage = false
        this.maxWireCost = 0
        this.plugins = []

        // init these, because they won't be read right away.
        this.revenuePerSec = null
        this.clipsSoldPerSec = null
        this.wirePerSpool = 1000
        this.saveForMarketing = false
        this.marketingBias = 1
    }

    process() {
        if (getStage() == 1 && optionTicked("autoPlayPhase1")) {
            this.updateValues();
            this.runPlugins()
            if (!this.doneWithStage) {
                eventLogger.logEvent("Stage1Status", this.getStatus())
            }

            if (projectVisible("projectButton35") && btnActive("projectButton35")) {
                this.doneWithStage = true
                eventLogger.logEventOnce("done with stage 1")
            }
            
            if (this.wire <= 1) 
                this.buyWire()

            if (this.trust > this.processors + this.memory)
                this.buyProcsOrMemory()

            if (btnActive('btnMakePaperclip')) 
                clipClick(1)

            activateAvailableProjects()
            this.considerIncreasingMarketing()
            this.considerBuyingClippers()
            this.manageInvestments()

            for (var i=0;i<clipMilestones.length;i++) {
                if (this.clips >= clipMilestones[i][0] && this.clips <= clipMilestones[i][1]) {
                    eventLogger.logEventOnce("milestone", {milestone:'clips>=' + clipMilestones[i][0], marketingBias:this.marketingBias}) 
                }
            }
        }
    }

    runPlugins() {
        for (var i=0;i<this.plugins.length;i++) {
            try {
                this.plugins[i].execute()
            } catch(err) {
                console.log("error running plugin", this.plugins[i], err)
            }
        }
    }

    manageInvestments() {
        if (divVisible("investmentEngine")) {
            this._investmentStrategyDecisions()
            this._investmentMoneyDecisions()
        }
    }

    _investmentStrategyDecisions() {
        // Decisions about strategy - not money
        if (this.investmentLevel < 9 && btnActive('btnImproveInvestments')) 
            investUpgrade()

        if (this.investmentLevel>=5) {
            document.getElementById('investStrat').value = 'hi'
        } else if (this.investmentLevel>=3) {
            document.getElementById('investStrat').value = 'med'
        } else {
            document.getElementById('investStrat').value = 'low'
        }
    }

    _investmentMoneyDecisions(){
        if (this.funds + this.portValue > 10000000000) { //10,000,000,000 - way more than enough for everything that requires money.
            document.getElementById('investStrat').value = 'low'
            if (btnActive('btnWithdraw')) investWithdraw()
        } else if (this.getNetWorth() < this.maxWireCost) {
            // right now, it's possible for me to end up with massively negative funds.  I think this is happening because manageInvestments and 
            // is investing in the same timeframeas a large purchase happening, and due to the way Paperclips 'takes turns' instead of being fully real-time, 
            // it lets my money go negative. 
            // This is a hack until I figure out how to address that gap.  If my available funds fall below the cost of a spool of wire, withdraw money
            eventLogger.logEvent("FundsEmergency",{funds:this.funds, portValue:this.portValue} )
            if (btnActive('btnWithdraw')) investWithdraw()
        } else {

            for (var i=0;i<reasonsToWithdraw.length;i++) {
                const reason = reasonsToWithdraw[i]
                if (projectVisible(reason.projectId) && !btnActive(reason.projectId) && this.portValue >= reason.requiredCash) {
                    eventLogger.logEventOnce("CashOut:"+ getProject(reason.projectId).title +" is available")
                    if (btnActive('btnWithdraw')) {
                        investWithdraw()
                        // bail out - we don't really want to invest.
                        return
                    }
                } 
            }

            if (this.investmentLevel >=1 && this.funds>this.portValue && btnActive('btnInvest')) {
                //buy some wire before investing - cut down on the chance of a funds emergency
                this.buyWire()
                investDeposit()
            }
        }
    }

    buyWire() {
        if (btnActive('btnBuyWire')) {
            var oldWire = this.wire
            buyWire()
            this.updateWire()
            this.wirePerSpool=this.wire - oldWire
        }
    }


    buyProcsOrMemory() {
        if (this.memory<3) 
            addMem()
        else if (this.processors<3)
            addProc()
        else if (this.memory<10)
            addMem()
        else
            addProc()
    }

    considerIncreasingMarketing() {
        if (btnActive('btnExpandMarketing')) {
            if (this.affordable(this.adCost)) {
                buyAds()
                this.saveForMarketing = false
            }
        } else {
            if (this.getNetWorth() > this.adCost * this.marketingBias) {
                this.saveForMarketing = true
            }
        }
    }

    considerBuyingClippers() {
        if (!this.saveForMarketing) {
            if (btnActive('btnMakeMegaClipper')) {
                if (this.affordable(this.megaClipperCost)) {
                    makeMegaClipper()
                }
            } else if (btnActive('btnMakeClipper') && this.autoClipperLevel < 75) {
                if (this.affordable(this.clipperCost)) {
                    makeClipper()
                }
            }
        }
    }

    affordable(purchaseCost) {
        // compares cost of item being considered to funds available, inventory of clips, and cost of a spool of wire.
        var netWorth = this.getNetWorth()
        var discretionaryFunds = netWorth - this.maxWireCost

        return discretionaryFunds >= purchaseCost
    }

    getNetWorth() {
        return this.funds + this.unsoldClips * this.getMinSalePrice()
    }

    updateValues() {
        this.now = new Date().valueOf();

        this.clips = getNumber('clips')
        this.funds = getNumber('funds')
    
        if (divVisible('revPerSecDiv')) {  // these tend to throw NaNs when the div isn't available
            this.revenuePerSec = getNumber('avgRev')
            this.clipsSoldPerSec = getNumber('avgSales')
        }

        this.unsoldClips = getNumber('unsoldClips')
        this.pricePerClip = getNumber('margin')
        this.publicDemand = getNumber('demand')

        this.marketingLvl = getNumber('marketingLvl')
        this.adCost = getNumber('adCost')
        this.clipmakerRate = getNumber('clipmakerRate')
        this.updateWire()
        this.wireCost = getNumber('wireCost')
        this.autoClipperLevel = getNumber('clipmakerLevel2')
        this.clipperCost = getNumber('clipperCost')
        this.megaClipperLevel = getNumber('megaClipperLevel')
        this.megaClipperCost = getNumber('megaClipperCost')

        this.trust = getNumber('trust')
        this.processors = getNumber('processors')
        this.memory = getNumber('memory')
        this.operations = getNumber("operations")
        this.maxOps = getNumber("maxOps")
        this.creativity = getNumber("creativity")

        this.investStrat = document.getElementById('investStrat').value
        this.investmentBankroll = getNumber('investmentBankroll')
        this.secValue = getNumber('secValue')
        this.portValue = getNumber('portValue')
        this.investmentLevel = getNumber('investmentLevel')

        this.yomiDisplay = getNumber('yomiDisplay')

        this.maxWireCost = Math.max(this.maxWireCost, this.wireCost)
    }

    updateWire() {
        this.wire = getNumber('wire')
    }

    getMinSalePrice() {
        return Math.ceil(this.maxWireCost*100 / this.wirePerSpool)/100 
    }

    lowerPrice() {
        if (btnActive('btnLowerPrice') && this.pricePerClip > this.getMinSalePrice()) 
            lowerPrice()
    }

    raisePrice() {
        if (btnActive('btnRaisePrice') && this.pricePerClip < MAX_SALE_PRICE) 
            raisePrice()
    }

    getStatus() {
        return {
            clips: this.clips,
            funds: this.funds,
            unsoldClips: this.unsoldClips,

            clipsSoldPerSec: this.clipsSoldPerSec,
            clipsSoldPerSec2: this.clipsSoldPerSec2,
            revenuePerSec: this.revenuePerSec,
            marketingLvl: this.marketingLvl,
            pricePerClip: this.pricePerClip,
            publicDemand: this.publicDemand,

            clipmakerRate: this.clipmakerRate,
            wire: this.wire,
            wireCost: this.wireCost,
            wirePerSpool: this.wirePerSpool,

            autoClipperLevel: this.autoClipperLevel,
            clipperCost: this.clipperCost,
            megaClipperLevel: this.megaClipperLevel,
            megaClipperCost: this.megaClipperCost,

            trust: this.trust,
            processors: this.processors,
            memory: this.memory,
            operations: this.operations,
            maxOps: this.maxOps,
            creativity: this.creativity,

            investmentBankroll: this.investmentBankroll,
            secValue: this.secValue,
            portValue: this.portValue,
            investStrat: this.investStrat,
            investmentLevel: this.investmentLevel,
            yomiDisplay: this.yomiDisplay
        }
    }
}
