import {btnActive,projectVisible,setWorkThink,optionTicked,getText,getStage} from './helper-functions.js'
import {eventLogger} from './json-event-logger.js'
import {ConsoleJsonAppender} from './console-json-appender.js'
import {ElasticJsonAppender} from './elastic-json-appender.js'
import {JsonStatusLogger} from './json-status-logger.js'

/* Paperclip globals */
/* global makeBattery,makeFactory,makeFarm,makeHarvester,makeWireDrone, */


// Phase 2
//////////////////////////
//  auto-plays stage 2  //
//////////////////////////
const SOLAR_BUTTONS = {
    1: 'btnMakeFarm',
    10: 'btnFarmx10',
    100:'btnFarmx100'
}

const STORAGE_BUTTONS = {
    1: 'btnMakeBattery',
    10: 'btnBatteryx10',
    100:'btnBatteryx100'
}

const WIRE_DRONE_BUTTONS = {
    1: 'btnMakeWireDrone',
    10: 'btnWireDronex10',
    100:'btnWireDronex100',
    1000:'btnWireDronex1000'
}

const HARVESTER_BUTTONS = {
    1: 'btnMakeHarvester',
    10: 'btnHarvesterx10',
    100:'btnHarvesterx100',
    1000:'btnHarvesterx1000'
}

const MAX_SOLAR_LOAD_WHEN_CHARGING = .78
const MAX_SOLAR_LOAD_WHEN_FULLY_CHARGED = .95
const MAX_REQUIRED_SOLAR_STORAGE = 10000000

const DRONE_COUNT_THRESHHOLD_X10 = 550
const DRONE_COUNT_THRESHHOLD_X100 = 1200
const DRONE_COUNT_THRESHHOLD_X1000 = 10000

class DroneWorld {
    constructor () {
        this.consumption = 0;
        this.production = 0;
        this.storedPower = 0;
        this.maxStorage = 0;

        this.availableMatter = 0;
        this.acquiredMatter = 0;
        this.acquiredWire = 0;

        this.multiplier = 1;

        this.harvesterDrones = 0;
        this.wireDrones = 0;

        this.workThink = 0;
    }

    process() {
        if (getStage() == 2 && optionTicked("autoPlayPhase2")) {
            this.updateValues();
            if (this.availableMatter > 0 || this.acquiredMatter > 0 || this.acquiredWire > 0) {
                this.takeAction();
                this.statusLogger.logStatus(this)
            } else {
                eventLogger.logEventOnce("done with stage 2")
                this.think()
            }
        }
    }

    /* ************************************************************ */
    /* ************************************************************ */

    updateValues() {
        this.consumption = parseFloat(getText('powerConsumptionRate'))
        this.production = parseFloat(getText('powerProductionRate'))
        this.storedPower = parseFloat(getText('storedPower'))
        this.maxStorage = parseFloat(getText('maxStorage'))

        this.acquiredMatter = parseFloat(getText('acquiredMatterDisplay'));
        this.availableMatter = parseFloat(getText('availableMatterDisplay'));

        this.acquiredWire = parseFloat(getText('nanoWire'));

        this.harvesterDrones = parseFloat(getText('harvesterLevelDisplay'));
        this.wireDrones = parseFloat(getText('wireDroneLevelDisplay'));

        this.workThink = document.getElementById("slider").value;

        this.setMultiplier();
    }

    setMultiplier() {
        this.multiplier = 1;
        this.droneMultiplier = 1;

        if (this.harvesterDrones + this.wireDrones > DRONE_COUNT_THRESHHOLD_X10) {
            this.multiplier = 10;
            this.droneMultiplier = 10;
        }
        if (this.harvesterDrones + this.wireDrones > DRONE_COUNT_THRESHHOLD_X100) {
            this.multiplier = 100;
            this.droneMultiplier = 100;
        }
        if (this.harvesterDrones + this.wireDrones > DRONE_COUNT_THRESHHOLD_X1000) {
            this.multiplier = 100;
            this.droneMultiplier = 1000;
        }
    }

    /* ************************************************************ */
    /* ************************************************************ */

    work() {
		// if drones can help, set them to working. But sometimes the drones can't help.  If there's available matter or aquired matter, they have work to do.  
		// if there's a surplus of wire, the factories are really the ones that need to work, and they always do - the work/think slider doesn't affect them.
		if ((this.availableMatter> 0 || this.acquiredMatter > 0) && this.acquiredWire == 0) {
			setWorkThink(10)
			eventLogger.logEventOnce('shifting attention to working')
		}
    }

    think() {
        setWorkThink(190) // 95% think
        eventLogger.logEventOnce('shifting attention to thinking')
    }

    takeAction() {
        //console.log("projectButton102 : " + projectVisible("projectButton102"));
        if (projectVisible("projectButton102")) {
            // Self-correcting Supply Chain - (1 sextillion clips) - Each factory added to the network increases every factory's output 1,000x
            // Best to stop buying, and wait for enough chips to buy this.
            if (btnActive("projectButton102")) {
                document.getElementById("projectButton102").click();
                this.think()
            } else {
                eventLogger.logEventOnce('...waiting for projectButton102 (Self-Correcting Supply Chain) to be clickable')
                this.work()
            }
        } else {
            this.think()
            if (this.powerOk()) {
                //console.log("acquiredMatter:" + this.acquiredMatter)
                //console.log("acquiredWire:" + this.acquiredWire)
                if (this.acquiredWire > 0) {
                    this.buyFactory()
                } else if (this.acquiredMatter > 0) {
                    this.buyWireDrone()
                } else if (this.acquiredMatter == 0 && this.availableMatter > 0) {
                    this.buyHarvesterDrone()
                } else {
                    // wait... What?   Acquired Wire is 0 ? And Acquired Matter is 0?  And there's not matter available?  Are we done?
                    if (this.availableMatter == 0 && this.acquiredMatter == 0 && this.acquiredWire == 0) {
                        // ok, then, I think we're done.  Why are we still here?  Either project46 isn't being autoactivated, or we need to sell some factories (to get enough clips)
                        if (projectVisible("projectButton46") && !btnActive("projectButton46")) {
                            if (btnActive('btnFactoryReboot')) 
                                document.getElementById("btnFactoryReboot").click()
                        }
                    }

                }
            } else {
                this.buySolar()

                // Special condition at start of level - have to bootstrap even with insufficient solar production.
                if (this.harvesterDrones == 0) this.buyHarvesterDrone()
                if (this.wireDrones == 0) this.buyWireDrone()
            }
            if (this.needMoreStorage()) {
                this.buyStorage()
            }
        }
    }

    powerOk() {
        var load = this.consumption / this.production;
        if (this.needSurplusPower())
            return load < MAX_SOLAR_LOAD_WHEN_CHARGING;
        else
            return load < MAX_SOLAR_LOAD_WHEN_FULLY_CHARGED;
    }

    needSurplusPower() {
        return this.storedPower < this.maxStorage;
    }

    needMoreStorage() {
        return this.storedPower == this.maxStorage && this.maxStorage < MAX_REQUIRED_SOLAR_STORAGE;
    }

    buyFactory() {
        if (btnActive('btnMakeFactory')) {
            eventLogger.logEvent("buying factory")
            makeFactory()
        } else {
            eventLogger.logEvent('...waiting to buy factory')
            this.work()
        }
    }

    buyWireDrone() {
        if (btnActive(WIRE_DRONE_BUTTONS[this.droneMultiplier])) {
            eventLogger.logEvent("buying " + this.droneMultiplier +" wire drone(s)")
            makeWireDrone(this.droneMultiplier)
        } else {
            eventLogger.logEvent('...waiting to buy ' + this.droneMultiplier + ' wire drone(s)')
            this.work()
        }
    }

    buyHarvesterDrone() {
        if (btnActive(HARVESTER_BUTTONS[this.droneMultiplier])) {
            eventLogger.logEvent("buying " + this.droneMultiplier +" harvester(s)")
            makeHarvester(this.droneMultiplier)
        } else {
            eventLogger.logEvent('...waiting to buy ' + this.droneMultiplier + ' harvester(s)')
            this.work()
        }
    }

    buySolar() {
        if (btnActive(SOLAR_BUTTONS[this.multiplier])) {
            eventLogger.logEvent("buying " + this.multiplier +" solar farm(s)")
            makeFarm(this.multiplier);
        } else {
            eventLogger.logEvent('...waiting to buy ' + this.multiplier + ' solar farm(s)')
            this.work()
        }
    }

    buyStorage() {
        if (btnActive(STORAGE_BUTTONS[this.multiplier])) {
            eventLogger.logEvent("buying " + this.multiplier +" battery(ies)")
            makeBattery(this.multiplier);
        } else {
            eventLogger.logEvent('...waiting to buy ' + this.multiplier + ' battery(ies)')
            this.work()
        }
    }
}
var dw = new DroneWorld();

dw.statusLogger = new JsonStatusLogger("Stage 2 Status Update", ['consumption','production','storedPower','maxStorage','availableMatter','acquiredMatter','acquiredWire','multiplier','harvesterDrones','wireDrones','workThink'])
dw.statusLogger.logTargets.push(new  ConsoleJsonAppender(function() { return optionTicked('logDWStats') } ))
dw.statusLogger.logTargets.push(new  ElasticJsonAppender(function() { return optionTicked('logDWStatsElas') } ))

window.setInterval(dw.process.bind(dw), 500);
