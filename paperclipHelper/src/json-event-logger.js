import {ConsoleJsonAppender} from './console-json-appender.js'
import {ElasticJsonAppender} from './elastic-json-appender.js'

export class JsonEventLogger {
    constructor() {
        this.start = null
        this.appenders = []
        this.filteredAppenders = []
    }

    addAppender(appender) {
        this.appenders.push(appender)
    }

    addFilteredAppender(filter, appender) {
        this.filteredAppenders.push({filter:filter, appender:appender})
    }

    getTimestamp() {
        return (new Date().valueOf()-this.start)/1000
    }

    log(obj) {
        var coveredByFilteredAppender = false
        this.filteredAppenders.forEach(function(filteredAppender) {
            if (filteredAppender.filter(obj)) {
                filteredAppender.appender.log(obj)
                coveredByFilteredAppender = true
            }
        }) 
        if (!coveredByFilteredAppender) {
            this.appenders.forEach(function(appender) {
                appender.log(obj)
            })
        }
    }

    logWithElapsedTime(object) {
        if (this.start == null) this.start = new Date().valueOf()
        var ts = {
            timestamp: new Date().valueOf(),
            elapsedTime: this.getTimestamp(),
            executionStart: this.start,
        }
        Object.assign(ts, object)
        this.log(ts)
    }

    logEvent(event, optionalDetails) {
        var obj = { event: event.trim()}
        if (optionalDetails)
            obj.details = optionalDetails
        this.logWithElapsedTime(obj)
    }

    logEventOnce(event, optionalDetails) {
        if (typeof this._logOnceMemory == 'undefined') {
            this._logOnceMemory = {}
        }
        if (typeof this._logOnceMemory[JSON.stringify(arguments)] == 'undefined') {
            this.logEvent(event, optionalDetails)
            this._logOnceMemory[JSON.stringify(arguments)] = JSON.stringify(arguments);
        }
    }

}

export var eventLogger = new JsonEventLogger()
eventLogger.addAppender(new  ConsoleJsonAppender())
eventLogger.addAppender(new  ElasticJsonAppender())
