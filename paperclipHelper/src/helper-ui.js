import {setWorkThink,getText,getStage} from './helper-functions.js'

//////////////////////
// helper interface //
//////////////////////

'use strict'

const UNUSED_SAVE_SLOT_NAME = "_--_-****new****-_--_";

//////////////////////////////////////////////////////
// Initialize local storage for saving helper state //
//////////////////////////////////////////////////////
var saveSlots = null;
try {
    saveSlots = JSON.parse(localStorage.getItem("saveSlots"));
} catch (err) {
    console.log("Error restoring saveSlots:" + err.message)
}
//window.saveSlots = saveSlots;
if (saveSlots === null) {
    saveSlots = {};
    //localStorage.setItem("saveSlots", JSON.stringify(saveSlots));
}

//////////////////////////////
// Build and inject the UI  //
//////////////////////////////

var helperDiv = document.createElement("div");
helperDiv.id="helper";

var helperHTML= "Helpers<hr>General<br><i>not needed if using autoplay</i><br>";
helperHTML += "<input class='savable-helper' id='autoAddProcs' type='checkbox' title='Not needed if using autoplays'>Auto-add processors</input><br>";
helperHTML += "<input class='savable-helper' id='autoAddMem' type='checkbox' title='Not needed if using autoplays'>Auto-add memory</input><br>";
helperHTML += "<input class='savable-helper' id='activateAvailableProjects' type='checkbox' title='Already included in autoplays (1,2,3)'>Auto-activate key projects</input><br>";
helperHTML += "<hr>Not already covered by autoplay<br>";
helperHTML += "<input class='savable-helper' id='autoQuantum' type='checkbox'>Auto-quantum compute</input><br>";
helperHTML += "<input class='savable-helper' id='autoTourney' type='checkbox'>Auto-tourney</input><br>";
helperHTML += "<input class='savable-helper' id='monitorSwarm' type='checkbox'>Monitor swarm (entertain/sync)</input><br>";

helperHTML += "<hr>Phase 1<br>";
helperHTML += "<input class='savable-helper' id='autoPlayPhase1' type='checkbox'>Auto-play phase 1</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logS1Stats' type='checkbox'>log status to console</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logS1StatsElas' type='checkbox'>log status to Elastic</input><br>";
helperHTML += "<input class='savable-helper' id='autoMakePaperclips' type='checkbox'>Auto-make paperclips</input><br>";
helperHTML += "<input class='savable-helper' id='autoBuyWire' type='checkbox'>Auto-buy wire</input><br>";

helperHTML += "<hr>Phase 2<br>";
helperHTML += "<input class='savable-helper' id='autoPlayPhase2' type='checkbox'>Auto-play phase 2</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logDWStats' type='checkbox'>log status to console</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logDWStatsElas' type='checkbox'>log status to Elastic</input><br>";

helperHTML += "<hr>Phase 3<br>";
helperHTML += "<input class='savable-helper' id='autoPlayPhase3' type='checkbox'>Auto-play phase 3</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='ignoreCombat' type='checkbox' title='let probes get slaughtered in exchange for higher self-rep rate'>ignore combat</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logSEStats' type='checkbox'>log status to console</input><br>";
helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logSEStatsElas' type='checkbox'>log status to Elastic</input><br>";
helperHTML += "<input class='savable-helper' id='autoLaunchProbe' type='checkbox'>Auto-launch probe</input><br>";

helperHTML += "<hr>Save / Load Games<br>";
helperHTML += "<div id='gameSaverDiv'>";
helperHTML += getSaveSlotSelectorHtml(saveSlots);
helperHTML += "</div>";
helperHTML += "<button id='btnGameLoader'>load</button> <button id='btnGameSaver'>save</button> <button id='btnGameSaveClearer'>erase</button><br>";
helperHTML += "<button id='btnReset'>reset</button><br>";

helperDiv.innerHTML=helperHTML;

document.getElementById("page").appendChild(helperDiv);
document.getElementById("helper").style.float = "left";
document.getElementById("helper").style.marginLeft = "3px";
document.getElementById("helper").style.width = "280px";


const SAVABLE_HELPERS =(function () {
	var savableHelperElements = document.getElementsByClassName('savable-helper')
	var savableHelpers = []
	for (var i=0;i<savableHelperElements.length;i++) {
		var e = savableHelperElements[i];
		if (e.type == 'checkbox') {
			savableHelpers.push(e.id)
		}
	}
	return savableHelpers
})()

function getSaveSlotSelectorHtml(saveSlots) {
    var selectedSaveSlot = localStorage.getItem("helperCurrentSaveSlot");
    if (selectedSaveSlot === null) selectedSaveSlot = UNUSED_SAVE_SLOT_NAME;

    var html = "<select id='saveSlotSelector'>";

    html += getSaveSlotSelectorOptionsHtml(saveSlots, selectedSaveSlot)
    html += "</select>";
    return html;
}

function getSaveSlotSelectorOptionsHtml(saveSlots, selectedSaveSlot) {
    var html = "";
    var selected = "";
    var saveSlotsKeys = Object.keys(saveSlots);
    saveSlotsKeys.sort()

    for (var i = 0 ; i < saveSlotsKeys.length ; i++) {
        const slotName = saveSlotsKeys[i];
        selected = "";
        if (selectedSaveSlot === slotName) selected = " selected";
        html += "<option value='" + slotName + "'" + selected + ">" + slotName + "</option>";
    }

    if (selectedSaveSlot === UNUSED_SAVE_SLOT_NAME) selected = " selected"
    else selected = "";

    html += "<option value='" + UNUSED_SAVE_SLOT_NAME+ "'" + selected + ">empty slot</option>";

    return html
}

function updateSaverDiv() {
    document.getElementById("gameSaverDiv").innerHTML = getSaveSlotSelectorHtml(saveSlots);
}

function getSelectedSlotName() {
    return document.getElementById('saveSlotSelector').value;
}

function writeToOrClearLocalStorageKey(key, value) {
    if (value == null) {
        console.log('removing value for key: ' + key)
        localStorage.removeItem(key)
    } else {
        localStorage.setItem(key, value)
    }
}

function loadSavedGame() {
    var slotName = getSelectedSlotName();
    if (slotName == UNUSED_SAVE_SLOT_NAME) {
        alert("Invalid slot selection.\nCan't load the empty slot.");
    } else {
        var savedGame = saveSlots[slotName];
        console.log(savedGame);

        console.log("Loading slot : " + slotName);

        writeToOrClearLocalStorageKey('saveGame', savedGame.saveGame)
        writeToOrClearLocalStorageKey('savePrestige', savedGame.savePrestige)
        writeToOrClearLocalStorageKey('saveProjectsActive', savedGame.saveProjectsActive)
        writeToOrClearLocalStorageKey('saveProjectsFlags', savedGame.saveProjectsFlags)
        writeToOrClearLocalStorageKey('saveProjectsUses', savedGame.saveProjectsUses)
        writeToOrClearLocalStorageKey('saveStratsActive', savedGame.saveStratsActive)
        writeToOrClearLocalStorageKey("helperSaveSlider", savedGame.helperSaveSlider)
        writeToOrClearLocalStorageKey("helperCurrentSaveSlot", slotName)
        writeToOrClearLocalStorageKey("helperActiveHelpers", savedGame.helperActiveHelpers)
        window.location.reload();
    }
}
document.getElementById('btnGameLoader').addEventListener("click", loadSavedGame);

function getDefaultSaveName() {
    return "u" + getText('prestigeUcounter') + "/s" + getText('prestigeScounter') + " - stage " + getStage()
}

function saveGame() {
    var slotName = getSelectedSlotName();

    var savedGame = {
        'saveGame':localStorage.getItem("saveGame"),
        'savePrestige':localStorage.getItem("savePrestige"),
        'saveProjectsActive':localStorage.getItem("saveProjectsActive"),
        'saveProjectsFlags':localStorage.getItem("saveProjectsFlags"),
        'saveProjectsUses':localStorage.getItem("saveProjectsUses"),
        'saveStratsActive':localStorage.getItem("saveStratsActive"),
        'helperSaveSlider':localStorage.getItem("helperSaveSlider"),
        'helperActiveHelpers':localStorage.getItem("helperActiveHelpers")
    };
    while (slotName == UNUSED_SAVE_SLOT_NAME) {
        var defaultName = getDefaultSaveName() + " - " + new Date().toISOString();
        slotName = prompt("Enter slot name", defaultName);
    }
    if (slotName) {
        if (!saveSlots.hasOwnProperty(slotName) || confirm("Update saved game in slot '" + slotName + "'?")) {
            console.log("saving to " + slotName);
            saveSlots[slotName] = savedGame;
            localStorage.setItem("saveSlots", JSON.stringify(saveSlots));
            localStorage.setItem("helperCurrentSaveSlot", slotName)
            updateSaverDiv();
        }
    } else {
        console.log("did not save");
    }
}
document.getElementById('btnGameSaver').addEventListener("click", saveGame);

function clearSavedGame() {
    var slotName = getSelectedSlotName();

    if (slotName == UNUSED_SAVE_SLOT_NAME) {
        alert("Invalid slot selection.\nCan't clear the empty slot.");
    } else {
        if (confirm("Clear the saved game slot '" + slotName + "'?")) {
            console.log("clearing saved game:" + slotName);
            delete saveSlots[slotName];
            localStorage.setItem("saveSlots", JSON.stringify(saveSlots));
            updateSaverDiv();
        } else {
            console.log("did not clear.");
        }
    }
}
document.getElementById('btnGameSaveClearer').addEventListener("click", clearSavedGame);

function resetGame() {
    if (confirm("Reset Game?")) {
        reset();  // eslint-disable-line
    }
}
document.getElementById('btnReset').addEventListener("click", resetGame);

///////////////////////////
// Helper Restore State  //
///////////////////////////
function restoreCustomSaves() {
    var sliderVal = localStorage.getItem("helperSaveSlider");
    if (sliderVal !== null) {
        setWorkThink(sliderVal)
    }
    var checkedHelpersJson = localStorage.getItem("helperActiveHelpers")
    var checkedHelpers = null;
    if (checkedHelpersJson) {
        try {
            checkedHelpers = JSON.parse(checkedHelpersJson)
        } catch (err) {
            console.log("Error parsing helperActiveHelpers" + err.message)
        }
    }
    if (checkedHelpers) {
        activateHelpers(checkedHelpers);
    }
}

function activateHelpers(helpers) {
    for (let i = 0; i < helpers.length; i++) {
        if (SAVABLE_HELPERS.indexOf(helpers[i]) != -1) {
            document.getElementById(helpers[i]).checked = true
        }
    }
}

restoreCustomSaves()

////////////////////////
// Helper Save State  //
////////////////////////
function saveCustomSaves() {
    var sliderVal = document.getElementById("slider").value
    var checkedHelpers = JSON.stringify(getActiveHelpers())
    localStorage.setItem("helperSaveSlider", sliderVal);
    localStorage.setItem("helperActiveHelpers", checkedHelpers);
    //console.log(checkedHelpers)
    //console.log(sliderVal)
}

function getActiveHelpers() {
    var checkedHelpers = []
    for (let i = 0; i < SAVABLE_HELPERS.length; i++) {
        if (document.getElementById(SAVABLE_HELPERS[i]) && document.getElementById(SAVABLE_HELPERS[i]).checked) {
            checkedHelpers.push(SAVABLE_HELPERS[i])
        }
    }
    return checkedHelpers
}

document.getElementById('slider').addEventListener("change", saveCustomSaves);
for (let i = 0; i < SAVABLE_HELPERS.length; i++) {
    document.getElementById(SAVABLE_HELPERS[i]).addEventListener("change", saveCustomSaves);
}





