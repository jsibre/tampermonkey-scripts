/* TamperMonkey globals */
/* global GM_info */

export class JsonStatusLogger {
    constructor(eventName, terms) {
        this.start = new Date().valueOf()
        this.firstLog = true
        this.logTargets = []
        this.eventName = eventName
        this.terms = terms
    }

    getTimestamp() {
        return (new Date().valueOf()-this.start)/1000
    }

    logHeader() {
        this.logWithElapsedTime({
            event:"logStart",
            scriptName: GM_info.script.name,
            scriptVersion: GM_info.script.version
        })
    }
    
    logWithElapsedTime(object) {
        var ts = {
            elapsedTime: this.getTimestamp()
        }
        Object.assign(ts, object)
        this.log(ts)
    }

    log(obj) {
        this.logTargets.forEach(function(logger) {
            logger.log(obj)
        })
    }

    resolve(obj, terms) {
        if (terms.length == 1) {
            return obj[terms[0]]
        } else {
            var t = terms.shift()
            return this.resolve(obj[t], terms)
        }
    }

    logStatus(obj) {
        if (this.firstLog) {
            this.firstLog = false
            this.logHeader()
        }

        var output = {event:this.eventName}

        this.terms.forEach(function(term) {
            output[term] = this.resolve(obj, term.split("."))
        }.bind(this))

        this.logWithElapsedTime(output)
    }
}
