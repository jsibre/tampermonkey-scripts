import {optionTicked,btnActive,divVisible,activateAvailableProjects,getStage} from './helper-functions.js'

/* Paperclip globals */
/* global addMem,addProc */
/* global qChips,qComp, */
/* global newTourney,runTourney, */
/* global entertainSwarm,synchSwarm, */


function autoPlayingPhaseOne() {
    if (getStage() == 1 && optionTicked('autoPlayPhase1'))
        return true
    else
        return false
}


//////////////////////
//  autobuy memory  //
//////////////////////
window.setInterval(function() {
    if (optionTicked("autoAddMem") && !autoPlayingPhaseOne())
        if (btnActive('btnAddMem'))
            addMem();
}, 50);


//////////////////////////
//  autobuy processors  //
//////////////////////////
window.setInterval(function() {
    if (optionTicked("autoAddProcs") && !autoPlayingPhaseOne())
        if (btnActive('btnAddProc'))
            addProc();
}, 50);


///////////////////////////////////
//  automatic quantum computing  //
///////////////////////////////////
function compute(clicks) {
    for (var j=0; j<clicks; j++) {
        qComp()
    }
}

function qComp_predict(){
    var q = 0;

    for (var i = 0; i<qChips.length; i++){
        q = q+qChips[i].value;
    }
    return Math.ceil(q*360)
}

window.setInterval(function() {
    if (optionTicked("autoQuantum")) {
        if (qComp_predict() > 0) {
            compute(100);
        }
    }
}, 100);


///////////////////////////
//  autorun tournaments  //
///////////////////////////
window.setInterval(function() {
    if (optionTicked("autoTourney")) {
        if (divVisible('tournamentManagement') && btnActive('btnNewTournament'))
            newTourney()
        if (btnActive('btnRunTournament')) {
            var stratPicker = document.getElementById('stratPicker')
            if (stratPicker.options.length >= 5) {
                stratPicker.value = 3 //GREEDY
            } else if (stratPicker.value == 10) {
                stratPicker.value = stratPicker.options.length - 2
            }
            runTourney()
        }
    }
}, 1000);


///////////////////////////////////
//  activate available projects  //
///////////////////////////////////
window.setInterval(function() {
    if (optionTicked('activateAvailableProjects')) {
        activateAvailableProjects()
    }
},1000);



///////////////////////////////////
//  activate available projects  //
///////////////////////////////////
window.setInterval(function() {
    if (optionTicked('monitorSwarm')) {
        if (divVisible("entertainButtonDiv") && btnActive('btnEntertainSwarm')) entertainSwarm()
        if (divVisible("synchButtonDiv") && btnActive('btnSynchSwarm')) synchSwarm()
    }
},2000);

