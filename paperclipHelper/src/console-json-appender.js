export class ConsoleJsonAppender {
    constructor (filterFunction) {
        this.filterFunction = filterFunction
    }
    log(obj) {
        if (! this.filterFunction || this.filterFunction() ) {
            console.log(obj)
        }
    }
}
