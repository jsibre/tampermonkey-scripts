/* declare globals for eslint */
/*global unsafeWindow */

import {eventLogger} from './json-event-logger.js'

///////////////
//  helpers  //
///////////////
export function btnActive(elemId) {
    return ! document.getElementById(elemId).disabled
}

export function projectVisible(elemId) {
    var e = document.getElementById(elemId);
    if (e) {
        var sv = e.style.visibility
        return sv == "visible";
    } else {
        return false
    }
}

export function getProject(elemId) {
    var variableName = elemId.replace("Button","")
    return unsafeWindow[variableName]
}

export function setWorkThink(value) {
    document.getElementById("slider").value = value
}

export function divVisible(elemId) {
    return document.getElementById(elemId).style.display != 'none'
}

export function optionTicked(elemId) {
    return document.getElementById(elemId).checked
}

export function getText(elemId) {
    return document.getElementById(elemId).textContent.replace(/[,a-z ]/g,'')
}

export function getNumber(elemId) {
    try {
        return valueOf(document.getElementById(elemId).textContent.replace(/,/g,''))    
    } catch(err) {
        console.log("Error parsing value for element '"+elemId+"' ("+document.getElementById(elemId).textContent+"). " + err);
    }
    
}

export function valueOf(strValue) {
    var num = strValue.replace(/[a-z ]/ig,'')
    var multiplier = strValue.replace(/[-0-9. ]/g,'')
    return num * valueOfWord(multiplier)
}

export function valueOfWord(numberName) {
    if (numberName == '') return 1

    for (const [key, value] of Object.entries(MULTIPLIERS)) {
        if (numberName == value) return key;
    }

    console.log("############## Found a number I don't know: " + numberName);
    throw 'Could not interpret "'+numberName+'"'
}

var helperFlag_nextAllowableActivateAvailableProjects = 0;
export function activateAvailableProjects() {
    // let's make sure we don't buy projects too quickly - I think there's a lag in 'operations' being updated on-screen, combined with a gap in the way
    // paperclips game computes the abiltiy to afford a project vs the way it actually deducts the cost.   I think slowing down purchase rate will alleviate this, maybe.
    const now = new Date().valueOf()
    if (helperFlag_nextAllowableActivateAvailableProjects < now) {
        for (var i = 0 ; i <= IMMEDIATE_ACTION_PROJECTS.length ; i++) {
            var projectButtonId = IMMEDIATE_ACTION_PROJECTS[i];

            if (projectVisible(projectButtonId) && btnActive(projectButtonId)) {
                if (projectButtonId == 'projectButton133' && (projectVisible('projectButton132') || projectVisible('projectButton121')))
                    continue // skip 133 (threnodies) if 132 (monument to driftwar fallen) or 121 (Name the battles) is visible... save up for 132 or 121 instead.

                var project = getProject(projectButtonId)
                eventLogger.logEvent("ProjectActivated", {projectId: project.id, projectTitle: project.title.trim()})

                document.getElementById(projectButtonId).click();
                helperFlag_nextAllowableActivateAvailableProjects = now + 5000 // wait 5000ms before re-entry   
                return // only activate one at a time - they don't make sure you have enough moola to buy when you click, so you could 'cheat'
            }
        }
    }
}

export function getStage() {
    if (divVisible('businessDiv')) {
       return 1
    } else if (divVisible("spaceDiv")) {
        return 3
    } else {
        return 2
    }

}

const MULTIPLIERS = {
    1000 : "thousand",
    1000000 : "million",
    1000000000 : "billion",
    1000000000000 : "trillion",
    1000000000000000 : "quadrillion",
    1000000000000000000 : "quintillion",
    1000000000000000000000 : "sextillion",
    1000000000000000000000000 : "septillion",
    1000000000000000000000000000 : "octillion",
    1000000000000000000000000000000 : "nonillion",
    1000000000000000000000000000000000 : "decillion",
    1000000000000000000000000000000000000: "undecillion",
    1000000000000000000000000000000000000000: "duodecillion",
    1000000000000000000000000000000000000000000: "tredecillion",
    1000000000000000000000000000000000000000000000: "quattuordecillion",
    1000000000000000000000000000000000000000000000000: "quindecillion",
    1000000000000000000000000000000000000000000000000000: "sexdecillion",
    1000000000000000000000000000000000000000000000000000000: "septendecillion",
}


const IMMEDIATE_ACTION_PROJECTS = [
    // Stage 1
    "projectButton3",    // Creativity (1000). - appears when you have ops filled up
    "projectButton50",   // Quantum Computing. - comes when you get 100 Creativity?  (I was at ~324k clip)    **** Buy this ASAP
    "projectButton51",   // Photonic Chip (many times, 10,000 -> 55,000)    **** Buy this ASAP

    "projectButton20",   // Strategic Modeling
    "projectButton21",   // Algorithmic Trading (10,000)


    // "projectButton26",   // WireBuyer - not really needed with this Helper
    // "projectButton118",  // Auto-Tourney. (only appeared after buying first Token of goodwill). - Not needed with Helper
    // "projectButton42",   // RevTracker

    // Wire Extrusion Enhancements
    "projectButton7",   // Improved Wire Extrusion (1750) +50%
    "projectButton8",   // Optimized Wire Extrusion (3500) +75% 
    "projectButton9",   // Microlattic Shapecasting (7500) +100% 
    "projectButton10",  // Spectral Froth Annealment (12000) +200%
    "projectButton10b", // Quantum Foam Annealment

    // Auto Clippers
    "projectButton1",    // Improved AutoClippers. +25%
    "projectButton4",    // Even Better AutoClippers +50%
    "projectButton5",    // Optimized AutoClippers (5000) +75%
    "projectButton16",   // Hadwiger Clip Diagrams (6000) +500%



    // Trust
    "projectButton6",     // Limerick (+1)
    "projectButton13",    // Lexical Processing (+1)
    "projectButton14",    // Combinatory Harmonics (+1)
    "projectButton15",    // The Hadwiger Problem (+1)
    "projectButton17",    // The Toth Sausage Conjecture (+1)
    "projectButton19",    // Donkey Space (+1)

    "projectButton27",   // Coherent Extrapolated Volition (+1)
    "projectButton28",   // Cure for Cancer (+10)
    "projectButton29",   // World Peace (+12)
    "projectButton30",   // Global Warming (+15)
    "projectButton31",   // Male Pattern Baldness (+20)

    // Marketing boosts
    "projectButton11",   // New Slogan () 50%
    "projectButton12",   // Catchy Jingle () 100%
    "projectButton34",   // Hypno Harmonics
    "projectButton37",   // Hostile Takeover ($1,000,000)
    "projectButton38",   // Full Monopoly ($10,000,000)
    "projectButton70",   // Hypno Drones
    //"projectButton35",   // Release the Hypno Drones - 100 trust <- ******** this is what concludes stage 1 ******


    // Mega Clippers
    "projectButton22",   // Mega-Clippers
    "projectButton23",   // Improved Mega-Clippers
    "projectButton24",   // Even Better Mega-Clippers
    "projectButton25",   // Optimized Mega-Clippers

    // Strategic Modelling
    "projectButton60",   // New Strategy A100
    "projectButton61",   // New Strategy B100
    "projectButton62",   // New Strategy GREEDY. - REALLY not sure it's worth going beyond GREEDY until stage 3
    // "projectButton63",   // New Strategy GENEROUS
    // "projectButton64",   // New Strategy MINIMAX
    // "projectButton65",   // New Strategy TIT FOR TAT
    // "projectButton66",   // New Strategy BEAT LAST
    // "projectButton119",  // Theory of Mind (doesn't appear until after you buy BEAT LAST)


    // Tokens/Gifts (begin to appear shortly after 100,000,000 clips)
    "projectButton40",   // Token of Goodwill
    "projectButton40b",   // Another Token of Goodwill (repeats up until 100 trust, or $256,000,000, not sure which)
    // "projectButton219", // Xavier Re-initialization




    // Stage 2
    //"projectButton118", // auto-tourney  - Ignore, let the user do it.  It's low value, and costs 50,000
    "projectButton18",  // Tóth Tubule Enfolding - Technique for assembling clip-making technology directly out of paperclips
    "projectButton127", // Power Grid - Solar Farms for generating electrical power
    "projectButton41",  // Nanoscale Wire Production - Technique for converting matter into wire
    "projectButton43",  // Harvester Drones
    "projectButton44",  // Wire Drones
    "projectButton45",  // Clip Factories

    "projectButton125", // momentum
    "projectButton126", // swarm computing
    "projectButton110", // drone flocking: collision avoidance - 100x more effective
    "projectButton100", // upgraded factories - 100x more effective
    "projectButton101", // hyperspeed factories - 1000x more effective
    "projectButton111", // drone flocking: collision avoidance - 1000x more effective
    "projectButton112", // drone flocking: Adversarial Cohesion - each drone doubles every drone's output
    "projectButton46",  // Space Exploration



    // Stage 3
    "projectButton120", // The OODA Loop - Utilize Probe Speed to outmaneuver enemies in battle
    "projectButton121", // Name the Battles - Give each battle a unique name, increase max trust for probes
    "projectButton128", // Strategic Attachment - Gain bonus yomi based on the results of your pick
    "projectButton129", // Elliptic Hull Polytopes - Reduce damage to probes from ambient hazards
    "projectButton130", // Reboot the Swarm - Turn the swarm off and then turn it back on again
    "projectButton131", // Combat - Add combat capabilities to Von Neumann Probes
    "projectButton132", // Monument to the Driftwar Fallen
    "projectButton133", // Threnodies (reused several times)
    "projectButton134", // Glory - bonus honor for consequtive victory
    //"projectButton218", // Limerick (cont.) - If is follows ought, it'll do what they thought

]
