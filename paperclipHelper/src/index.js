
import './helper-ui.js' 		// just want the side effects, don't need a reference to anything.
import './helpers-general.js'	// just want the side effects, don't need a reference to anything.
import './helpers-stage-2.js'	// just want the side effects, don't need a reference to anything.
import './helpers-stage-3.js'	// just want the side effects, don't need a reference to anything.

import {optionTicked} from './helper-functions.js'
import {eventLogger} from './json-event-logger.js'
import {ConsoleJsonAppender} from './console-json-appender.js'
import {ElasticJsonAppender} from './elastic-json-appender.js'

import {StageOnePlayer} from './helpers-stage-1.js'	
import {/*TargetInventoryPriceAdjusterPlugin,*/ JamDetectorPlugin, /*ClipSalesPlugin,*/ AverageSalesPlugin, InventoryBoundsPriceAdjusterPlugin} from './plugins/stage-1.js'	


(function() {
	'use strict';

	var stageOnePlayer = new StageOnePlayer();
	eventLogger.addFilteredAppender(function(obj) {return obj.event == 'Stage1Status'}, new  ConsoleJsonAppender(function() { return optionTicked('logS1Stats') } ))
	eventLogger.addFilteredAppender(function(obj) {return obj.event == 'Stage1Status'}, new  ElasticJsonAppender(function() { return optionTicked('logS1StatsElas') } ))

	//stageOnePlayer.plugins.push(new TargetInventoryPriceAdjusterPlugin(stageOnePlayer, 0.03, 60, true))
	stageOnePlayer.plugins.push(new InventoryBoundsPriceAdjusterPlugin(stageOnePlayer, 0.03, .5, 2000))

	stageOnePlayer.plugins.push(new JamDetectorPlugin(stageOnePlayer))
	//stageOnePlayer.plugins.push(new ClipSalesPlugin(stageOnePlayer, 24, true))
	stageOnePlayer.plugins.push(new AverageSalesPlugin(stageOnePlayer, true))


	window.setInterval(stageOnePlayer.process.bind(stageOnePlayer), 330);


})();