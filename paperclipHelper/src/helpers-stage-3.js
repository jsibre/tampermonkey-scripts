
import {btnActive,setWorkThink,divVisible,optionTicked,getNumber,activateAvailableProjects} from './helper-functions.js'
import {ConsoleJsonAppender} from './console-json-appender.js'
import {ElasticJsonAppender} from './elastic-json-appender.js'
import {JsonStatusLogger} from './json-status-logger.js'
import {eventLogger} from './json-event-logger.js'

/* Paperclip globals */
/* global increaseMaxTrust,increaseProbeTrust, */
/* global makeProbe */
/* global lowerProbeCombat,lowerProbeFac,lowerProbeHarv,lowerProbeHaz,lowerProbeNav,lowerProbeRep,lowerProbeSpeed,lowerProbeWire,raiseProbeCombat,raiseProbeFac,raiseProbeHarv,raiseProbeHaz,raiseProbeNav,raiseProbeRep,raiseProbeSpeed,raiseProbeWire, */
/* global buttonUpdate,project120, */


// Phase 3
/////////////////////////
//  Auto-play Stage 3  //
/////////////////////////
const GROWTH_MODE_NON_COMBAT = {
    spd:0,
    exp:0,
    srp:"13w",  // 12w
    hrm:5,      // 8w
    fac:0,
    hvs:0,
    wir:0,
    cbt:0
}

const GROWTH_MODE_COMBAT = {
    spd:0,
    exp:0,
    srp:"10w",
    hrm:"5w",
    fac:0,
    hvs:0,
    wir:0,
    cbt:"5w"
}

const GROWTH_MODE_COMBAT_OODA = {
    spd:"2w",
    exp:0,
    srp:"10w",
    hrm:"5w",
    fac:0,
    hvs:0,
    wir:0,
    cbt:"3w"
}

const EXPLORE_BUILD_MODE_NON_COMBAT = {
    spd:3,      // 2
    exp:3,      // 2
    srp:"10w",  
    hrm:"8w",   
    fac:1,      
    hvs:2,
    wir:3,
    cbt:0
}

const EXPLORE_BUILD_MODE_COMBAT = {
    spd:3,      // 2
    exp:3,      // 2
    srp:"10w",
    hrm:"4w",
    fac:1,
    hvs:2,
    wir:3,
    cbt:"4w"
}

const HYPER_EXPLORE_MODE_COMBAT = {
    spd:"3w",
    exp:"3w",
    srp:4,
    hrm:6,
    fac:0,
    hvs:0,
    wir:0,
    cbt:6
}



class SpaceExploration {
    constructor () {
        this.doneWithStage = false
        this.lastCycleTime = 0;
        this.CYCLE_DELAY_MS = 12000
        this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PUE_THRESHOLD = .1
        this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PROBE_THRESHOLD = 4 * 10 ** 32
        this.PROBES_PER_LAUNCH = 2000
        this.AUTOLAUNCH_PROBES_IF_TOTAL_COUNT_LESS_THAN = 1500000
        this.SACRIFICE_PROBES_PRIOR_TO_COMBAT = 1000000000

        this.clipsPerSecond = 0
        this.unusedClips = 0

        this.matterAvailable = 0
        this.matterAcquired = 0
        this.wire = 0
        this.harvesterDrones = 0
        this.wireDrones = 0
        this.percentUniverseExplored = 0
        this.probesLaunched = 0
        this.probesBorn = 0
        this.probesLostToHazards = 0
        this.probesLostToValueDrift = 0
        this.probesLostInCombat = 0
        this.probesTotal = 0
        this.driftersKilled = 0
        this.drifters = 0

        this.operations = 0
        this.maxOps = 0
        this.creativity = 0
        this.trust = 0
        this.trustUsed = 0
        this.trustMax = 0
        this.probeSpeed = 0
        this.probeExploration = 0
        this.probeSelfReplication = 0
        this.probeHazardRemediation = 0
        this.probeFactoryProduction = 0
        this.probeHarvesterDroneProduction = 0
        this.probeWireDroneProduction = 0
        this.probeCombat = 0

        this.jumpstartLevel = 0

        this.probeTrustFunctions = {
            spd: {incr: function() {raiseProbeSpeed()} , decr: function() {lowerProbeSpeed()} },
            exp: {incr: function() {raiseProbeNav()} , decr: function() {lowerProbeNav()} },
            srp: {incr: function() {raiseProbeRep()} , decr: function() {lowerProbeRep()} },
            hrm: {incr: function() {raiseProbeHaz()} , decr: function() {lowerProbeHaz()} },
            fac: {incr: function() {raiseProbeFac()} , decr: function() {lowerProbeFac()} },
            hvs: {incr: function() {raiseProbeHarv()} , decr: function() {lowerProbeHarv()} },
            wir: {incr: function() {raiseProbeWire()} , decr: function() {lowerProbeWire()} },
            cbt: {incr: function() {raiseProbeCombat()} , decr: function() {lowerProbeCombat()} }
        }
    }

    process() {
        if (divVisible('spaceDiv')) {
            this.updateValues();

            if (optionTicked("autoPlayPhase3")) {

                this.autoLaunchProbes();
                this.chooseAndSetMode();
                this.setProbeTrustConfigurationModel()
                this.setProbeTrustConfiguration()

                if (this.unusedClips > 1000000000000000000 /* 1 Quintillion - way more than enough to buy probes @ 1 Quad */ ) {
                    setWorkThink(199) // 99.5% think
                } else {
                    setWorkThink(0)   // Working full time
                }
                activateAvailableProjects()
                
                if (!this.doneWithStage) {
                    if (btnActive("btnIncreaseProbeTrust")) increaseProbeTrust()
                    if (btnActive("btnIncreaseMaxTrust") && this.trust == this.trustMax) increaseMaxTrust()

                    this.statusLogger.logStatus(this)

                    if (this.percentUniverseExplored == 100 && this.matterAvailable == 0 && this.matterAcquired == 0 && this.wire == 0) {
                        this.doneWithStage = true
                        eventLogger.logEvent("done with stage 3")
                    }
                }
            }
        }
    }

    autoLaunchProbes() {
        if (this.probesTotal < this.AUTOLAUNCH_PROBES_IF_TOTAL_COUNT_LESS_THAN && btnActive('btnMakeProbe')) {
            for (var i=0; i<this.PROBES_PER_LAUNCH; i++) {
                makeProbe();
            }
        }
    }

    chooseAndSetMode() {
        if (this.percentUniverseExplored > this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PUE_THRESHOLD && this.probesTotal > this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PROBE_THRESHOLD) {
            this.mode = HYPER_EXPLORE_MODE_COMBAT
            eventLogger.logEventOnce("Force HYPER_EXPLORE_MODE_COMBAT selection!")
        } else {
            if (this.lastCycleTime + this.CYCLE_DELAY_MS < this.now) {
                this.lastCycleTime = this.now;

                if (!optionTicked('ignoreCombat') && this.isCombatEnabled() && this.probesLostInCombat > this.SACRIFICE_PROBES_PRIOR_TO_COMBAT) {
                    this.mode = EXPLORE_BUILD_MODE_COMBAT;
                } else {
                    this.mode = EXPLORE_BUILD_MODE_NON_COMBAT;
                }
            } else {
                if (!optionTicked('ignoreCombat') && this.isCombatEnabled() && this.probesLostInCombat > this.SACRIFICE_PROBES_PRIOR_TO_COMBAT) {
                    if (project120.flag) {
                        // we have OODA Loop
                        this.mode = GROWTH_MODE_COMBAT_OODA
                    } else {
                        this.mode = GROWTH_MODE_COMBAT
                    }
                } else {
                    this.mode = GROWTH_MODE_NON_COMBAT;
                }
            }
        }
    }

    setProbeTrustConfigurationModel() {
        var probeTrustConfiguration = {}
        var rawProbeTrustConfiguration = {}
        var sumOfWeights = this.getSumOfParameterWeights(this.mode)
        var sumOfNumerics = this.getSumOfParameterNumerics(this.mode)
        var maxWeighted = this.getMaxWeightedParameter(this.mode)

        //console.log('sum W :' + sumOfWeights)
        //console.log('sum N :' + sumOfNumerics)
        //console.log('mx key:' + maxWeighted)

        for (const [key, value] of Object.entries(this.mode)) {
            var v = null;
            if (typeof value != 'string') {
                v = value
                rawProbeTrustConfiguration[key] = v
            } else {
                v = getConfigWeight(value)/sumOfWeights * (this.trust - sumOfNumerics)
                rawProbeTrustConfiguration[key] = v
                if (key == maxWeighted) {
                    v = Math.ceil(v)
                } else {
                    v = Math.floor(v)
                }
                v = Math.round(v)
            }
            probeTrustConfiguration[key] = v
        }

        while (this.getSumOfParameterNumerics(probeTrustConfiguration) < this.trust) {
            probeTrustConfiguration[maxWeighted]++
        }

        //console.log(this.mode)
        //console.log(rawProbeTrustConfiguration)
        //console.log(probeTrustConfiguration)
        this.probeTrustConfigurationModel = probeTrustConfiguration;
    }


    setProbeTrustConfiguration() {
        var ptf = this.probeTrustFunctions;
        
        this.applyProbeTrustLogic(
            function(key, standard, currentProbeTrustConfiguration) {
                var ct = currentProbeTrustConfiguration[key] - standard
                while (ct > 0) {
                    ptf[key].decr();
                    currentProbeTrustConfiguration[key]--;
                    ct--;
                }
            }, this.probeTrustConfigurationModel)

        buttonUpdate();
        
        this.applyProbeTrustLogic(
            function(key, standard, currentProbeTrustConfiguration) {
                var ct = currentProbeTrustConfiguration[key] - standard
                while (ct < 0) {
                    ptf[key].incr();
                    currentProbeTrustConfiguration[key]++;
                    ct++;
                }
            }, this.probeTrustConfigurationModel)
    }

    applyProbeTrustLogic(executor, probeTrustConfigurationModel) {
        for (const [key, value] of Object.entries(probeTrustConfigurationModel)) {
            var i = 0;
            while(executor(key, value, this.probeTrustConfiguration) && i < 100) {
                i++
            }
        }
    }

    getSumOfParameterWeights(mode) {
        var sum = 0;

        for (const value of Object.values(mode)) {
            sum += getConfigWeight(value)
        }
        return sum
    }

    getSumOfParameterNumerics(mode) {
        var sum = 0;

        for (const value of Object.values(mode)) {
            if (typeof value == typeof 0) {
                sum += value
            }
        }
        return sum
    }

    getMaxWeightedParameter(mode) {
        var mx = 0;
        var keyMax = null;
        for (const [key, value] of Object.entries(mode)) {
            var v = getConfigWeight(value)
            if (v >= mx) {
                mx = v
                keyMax = key
            }
        }
        return keyMax
    }

    updateValues() {
        this.now = new Date().valueOf();
        this.clips = getNumber('clips')
        this.clipsPerSecond = getNumber('clipmakerRate2')
        this.unusedClips = getNumber('unusedClipsDisplay')
        this.matterAvailable = getNumber('availableMatterDisplay')
        this.matterAcquired = getNumber('acquiredMatterDisplay')
        this.wire = getNumber('nanoWire')
        this.harvesterDrones = getNumber('harvesterLevelSpace')
        this.wireDrones = getNumber('wireDroneLevelSpace')

        this.percentUniverseExplored = getNumber('colonizedDisplay')
        this.probesLaunched = getNumber('probesLaunchedDisplay')
        this.probesBorn = getNumber('probesBornDisplay')
        this.probesLostToHazards = getNumber('probesLostHazardsDisplay')
        this.probesLostToValueDrift = getNumber('probesLostDriftDisplay')
        this.probesLostInCombat = getNumber('probesLostCombatDisplay')
        this.probesTotal = getNumber('probesTotalDisplay')
        this.driftersKilled = getNumber('driftersKilled')
        this.drifters = getNumber('drifterCount')
        this.operations = getNumber("operations")
        this.maxOps = getNumber("maxOps")
        this.creativity = getNumber("creativity")
        this.trust = parseFloat(getNumber('probeTrustDisplay'))
        this.trustUsed = parseFloat(getNumber('probeTrustUsedDisplay'))
        this.trustMax = parseFloat(getNumber('maxTrustDisplay'))

        this.probeTrustConfiguration = {
            spd:getNumber('probeSpeedDisplay'),
            exp:getNumber('probeNavDisplay'),
            srp:getNumber('probeRepDisplay'),
            hrm:getNumber('probeHazDisplay'),
            fac:getNumber('probeFacDisplay'),
            hvs:getNumber('probeHarvDisplay'),
            wir:getNumber('probeWireDisplay'),
            cbt:getNumber('probeCombatDisplay')
        }

        this.probeSpeed = getNumber('probeSpeedDisplay')
        this.probeExploration = getNumber('probeNavDisplay')
        this.probeSelfReplication = getNumber('probeRepDisplay')
        this.probeHazardRemediation = getNumber('probeHazDisplay')
        this.probeFactoryProduction = getNumber('probeFacDisplay')
        this.probeHarvesterDroneProduction = getNumber('probeHarvDisplay')
        this.probeWireDroneProduction = getNumber('probeWireDisplay')
        this.probeCombat = getNumber('probeCombatDisplay')

        if (this.percentUniverseExplored > 0 && typeof this.universeMoved == 'undefined') {
            eventLogger.logEvent('universe just moved')
            this.universeMoved = true
        }
    }

    isCombatEnabled() {
        return divVisible('combatButtonDiv')
    }
}

function getConfigWeight(value) {
    var pattern = "([0-9.]+)w"
    if (typeof value == 'string') {
        if (value.match(pattern)) {
            return parseFloat(value.match(pattern)[1])
        }
        console.log("Error extracting weight from value '" + value + "'")
    }
    return 0
}

var se = new SpaceExploration();

se.statusLogger = new JsonStatusLogger('Stage 3 Status Update', ['clips','unusedClips','matterAvailable','matterAcquired','wire','percentUniverseExplored','probesLaunched','probesBorn','probesTotal','harvesterDrones','wireDrones','trust','probeTrustConfigurationModel.srp','operations','creativity'])
se.statusLogger.logTargets.push(new  ConsoleJsonAppender(function() { return optionTicked('logSEStats') } ))
se.statusLogger.logTargets.push(new  ElasticJsonAppender(function() { return optionTicked('logSEStatsElas') } ))

window.setInterval(se.process.bind(se), 300);

////////////////////////
//  autolaunch probe  //
////////////////////////
window.setInterval(function() {
    if (
        divVisible('spaceDiv') != 'none'
        &&
        optionTicked("autoLaunchProbe")
        &&
        ! optionTicked("autoPlayPhase3")
        &&
        btnActive('btnMakeProbe')) {
        for (var i=0; i<1000; i++) {
            makeProbe();
        }
    }
}, 100);
