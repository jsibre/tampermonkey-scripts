#!/bin/bash

usage() {
	echo "Usage :  commitAndLoad.sh \"commit message\""	
}

if [ -z "$1" ] ; then
	usage
	exit 1
fi

# pull bitbucket password from here
source .env

if [ -z "$BITBUCKET_APP_PASSWORD" -o -z "$BITBUCKET_REPO_OWNER" -o -z "$BITBUCKET_REPO_SLUG" ] ; then
	echo "Please provide proper environment variable values in .env file. (don't commit this file to the repo!!)"
	echo "Example:"
	echo "\$cat .env"
	echo "BITBUCKET_APP_PASSWORD=\"password goes here\""
	echo "BITBUCKET_REPO_OWNER=\"jsibre\""
	echo "BITBUCKET_REPO_SLUG=\"tampermonkey-scripts\""

	exit 1
fi

if npm run lint && npm run build ; then

	DIST_FILENAME=$(ls ./dist/*.user.js)
	DIST_BASENAME=$(basename "$DIST_FILENAME")

	echo
	echo
	git commit -am "$1"
	
	echo
	echo
	echo -n "Incrementing version to "
	npm version patch 


	echo
	echo
	echo "uploading $DIST_BASENAME to bitbucket" 
	postUrl="https://${BITBUCKET_REPO_OWNER}:${BITBUCKET_APP_PASSWORD}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads"
	curl -X POST "$postUrl" --form files=@"$DIST_FILENAME"

	echo
	echo
	echo "Updating userscript in Chrome" 
	/usr/bin/open -a "/Applications/Google Chrome.app" "https://bitbucket.org/jsibre/tampermonkey-scripts/downloads/$DIST_BASENAME"

	echo
	echo
	echo "pushing commit to bitbucket" 
	git push
else
	echo "errors above, aborting commit, push, and update"
fi
