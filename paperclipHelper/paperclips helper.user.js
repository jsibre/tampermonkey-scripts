// ==UserScript==
// @name         paperclips helper
// @namespace    paperclips helper
// @version      1.9.12
// @description  try to take over the world!  Script from github (below) tweaked and enhanced by Jason, and also converted to TamperMonkey script based off of https://github.com/takayatodoroki/paperclipsHelper
// @author       Jason & https://gist.github.com/stloewen/a8db68846489753b076527ae8da4ab94
// @match        https://www.decisionproblem.com/paperclips/index2.html
// @grant        GM_xmlhttpRequest
// @grant        unsafeWindow
// @connect      192.168.1.7
// @connect      *
// ==/UserScript==

(function() {
    'use strict';

    // var ticker=0;
    // var quantumdelay=3;

    const UNUSED_SAVE_SLOT_NAME = "_--_-****new****-_--_";

    const IMMEDIATE_ACTION_PROJECTS = [
        // Stage 1
        "projectButton20",   // Strategic Modelling
        "projectButton21",   // Algorithmic Trading 
        // "projectButton118",  // Auto-Tourney. (only appeared after buying first Token of goodwill). - Not needed with Helper
        
        "projectButton51",   // Photonic Chip (many times, 10,000 -> 55,000)
        "projectButton10b",  // Quantum Foam Annealment

        // Trust
        "projectButton27",   // Coherent Extrapolated Volition (+1)
        "projectButton28",   // Cure for Cancer (+10)
        "projectButton29",   // World Peace (+12)
        "projectButton30",   // Global Warming (+15)
        "projectButton31",   // Male Pattern Baldness (+20)

        // Marketing boosts
        "projectButton37",   // Hostile Takeover ($1,000,000)
        "projectButton38",   // Full Monopoly ($10,000,000)
        "projectButton70",   // Hypno Drones
        "projectButton35",   // Release the Hypno Drones - 100 trust <- ******** this is what concludes stage 1 ******


        // "projectButton26",   // Wire Buyer - not really needed with this Helper

        // Clippers
        "projectButton22",   // Mega-Clippers
        "projectButton23",   // Improved Mega-Clippers
        "projectButton24",   // Even Better Mega-Clippers
        "projectButton25",   // Optimized Mega-Clippers

        // Strategic Modelling
        "projectButton60",   // New Strategy A100
        "projectButton61",   // New Strategy B100
        "projectButton62",   // New Strategy GREEDY. - REALLY not sure it's worth going beyond GREEDY until stage 3
        // "projectButton63",   // New Strategy GENEROUS
        // "projectButton64",   // New Strategy MINIMAX
        // "projectButton65",   // New Strategy TIT FOR TAT
        // "projectButton66",   // New Strategy BEAT LAST
        // "projectButton119",  // Theory of Mind (doesn't appear until after you buy BEAT LAST)


        // Tokens/Gifts (begin to appear shortly after 100,000,000 clips)
        "projectButton40",   // Token of Goodwill
        "projectButton40b",   // Another Token of Goodwill (repeats up until 100 trust, or $256,000,000, not sure which)



        // "projectButton219", // Xavier Re-initialization


        // Considerations
        // Processors vs Memory...
        //   - get 
        // - must buy 75 auto-clippers
        // - 75 mega-clippers didn't seem to be significant
        // - must reach 101,000,000 clips to enable Token of Goodwill




        // Stage 2
        //"projectButton118", // auto-tourney  - Ignore, let the user do it.  It's low value, and costs 50,000
        "projectButton18",  // Tóth Tubule Enfolding - Technique for assembling clip-making technology directly out of paperclips
        "projectButton127", // Power Grid - Solar Farms for generating electrical power
        "projectButton41",  // Nanoscale Wire Production - Technique for converting matter into wire
        "projectButton43",  // Harvester Drones
        "projectButton44",  // Wire Drones
        "projectButton45",  // Clip Factories

        "projectButton125", // momentum
        "projectButton126", // swarm computing
        "projectButton110", // drone flocking: collision avoidance - 100x more effective
        "projectButton100", // upgraded factories - 100x more effective
        "projectButton101", // hyperspeed factories - 1000x more effective
        "projectButton111", // drone flocking: collision avoidance - 1000x more effective
        "projectButton112", // drone flocking: Adversarial Cohesion - each drone doubles every drone's output
        // Stage 3
        "projectButton120", // The OODA Loop - Utilize Probe Speed to outmaneuver enemies in battle
        "projectButton121", // Name the Battles - Give each battle a unique name, increase max trust for probes
        "projectButton128", // Strategic Attachment - Gain bonus yomi based on the results of your pick
        "projectButton129", // Elliptic Hull Polytopes - Reduce damage to probes from ambient hazards
        "projectButton130", // Reboot the Swarm - Turn the swarm off and then turn it back on again
        "projectButton131", // Combat - Add combat capabilities to Von Neumann Probes
        "projectButton132", // Monument to the Driftwar Fallen
        "projectButton133", // Threnodies (reused several times)
        "projectButton134", // Glory - bonus honor for consequtive victory
        //"projectButton218", // Limerick (cont.) - If is follows ought, it'll do what they thought

    ]


    //////////////////////////////////////////////////////
    // Initialize local storage for saving helper state //
    //////////////////////////////////////////////////////
    var saveSlots = null;
    try {
        saveSlots = JSON.parse(localStorage.getItem("saveSlots"));
    } catch (err) {
        console.log("Error restoring saveSlots:" + err.message)
    }
    //window.saveSlots = saveSlots;
    if (saveSlots === null) {
        saveSlots = {};
        localStorage.setItem("saveSlots", JSON.stringify(saveSlots));
    }

    //console.log("saveSlots:")
    //console.log(saveSlots)

    //////////////////////
    // helper interface //
    //////////////////////

    function updateSaverDiv() {
        document.getElementById("gameSaverDiv").innerHTML = getSaveSlotSelectorHtml(saveSlots);
    }

    var helperDiv = document.createElement("div");
    helperDiv.id="helper";

    var helperHTML= "Helpers<hr>General<br>";
    helperHTML += "<input class='savable-helper' id='autoAddProcs' type='checkbox'>Auto-add processors</input><br>";
    helperHTML += "<input class='savable-helper' id='autoAddMem' type='checkbox'>Auto-add memory</input><br>";
    helperHTML += "<input class='savable-helper' id='autoQuantum' type='checkbox'>Auto-quantum compute</input><br>";
    helperHTML += "<input class='savable-helper' id='autoTourney' type='checkbox'>Auto-tourney</input><br>";
    helperHTML += "<input class='savable-helper' id='activateAvailableProjects' type='checkbox'>Auto-activate key projects</input><br>";

    helperHTML += "<hr>Phase 1<br>";
    helperHTML += "<input class='savable-helper' id='autoMakePaperclips' type='checkbox'>Auto-make paperclips</input><br>";
    helperHTML += "<input class='savable-helper' id='autoBuyWire' type='checkbox'>Auto-buy wire</input><br>";

    helperHTML += "<hr>Phase 2<br>";
    helperHTML += "<input class='savable-helper' id='autoPlayPhase2' type='checkbox'>Auto-play phase 2</input><br>";
    helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logDWStats' type='checkbox'>log status to console</input><br>";

    helperHTML += "<hr>Phase 3<br>";
    helperHTML += "<input class='savable-helper' id='autoPlayPhase3' type='checkbox'>Auto-play phase 3</input><br>";
    helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='ignoreCombat' type='checkbox' title='let probes get slaughtered in exchange for higher self-rep rate'>ignore combat</input><br>";
    helperHTML += "&nbsp;&nbsp;<input class='savable-helper' id='logSEStats' type='checkbox'>log status to console</input><br>";
    helperHTML += "<input class='savable-helper' id='autoLaunchProbe' type='checkbox'>Auto-launch probe</input><br>";

    helperHTML += "<hr>Save / Load Games<br>";
    helperHTML += "<div id='gameSaverDiv'>";
    helperHTML += getSaveSlotSelectorHtml(saveSlots);
    helperHTML += "</div>";
    helperHTML += "<button id='btnGameLoader'>load</button> <button id='btnGameSaver'>save</button> <button id='btnGameSaveClearer'>erase</button><br>";

    helperDiv.innerHTML=helperHTML;

    document.getElementById("page").appendChild(helperDiv);
    document.getElementById("helper").style.float = "left";
    document.getElementById("helper").style.marginLeft = "3px";
    document.getElementById("helper").style.width = "280px";

    var savableHelpers = document.getElementsByClassName('savable-helper')
    console.log(savableHelpers)
    
    var SAVABLE_HELPERS = []
    for (var i=0;i<savableHelpers.length;i++) {
        var e = savableHelpers[i];
        if (e.type = 'checkbox') helpers.push(e.id)
    }

    ///////////////////////////
    // Helper Restore State  //
    ///////////////////////////
    function restoreCustomSaves() {
        var sliderVal = localStorage.getItem("helperSaveSlider");
        if (sliderVal !== null) {
            setWorkThink(sliderVal)
        }
        var checkedHelpersJson = localStorage.getItem("helperActiveHelpers")
        var checkedHelpers = null;
        if (checkedHelpersJson) {
            try {
                checkedHelpers = JSON.parse(checkedHelpersJson)
            } catch (err) {
                console.log("Error parsing helperActiveHelpers" + err.message)
            }
        }
        if (checkedHelpers) {
            activateHelpers(checkedHelpers);
        }
    }

    function activateHelpers(helpers) {
        for (let i = 0; i < helpers.length; i++) {
            if (SAVABLE_HELPERS.indexOf(helpers[i]) != -1) {
                document.getElementById(helpers[i]).checked = true
            }
        }
    }

    restoreCustomSaves()

    ////////////////////////
    // Helper Save State  //
    ////////////////////////
    function saveCustomSaves() {
        var sliderVal = document.getElementById("slider").value
        var checkedHelpers = JSON.stringify(getActiveHelpers())
        localStorage.setItem("helperSaveSlider", sliderVal);
        localStorage.setItem("helperActiveHelpers", checkedHelpers);
        //console.log(checkedHelpers)
        //console.log(sliderVal)
    }

    function getActiveHelpers() {
        var checkedHelpers = []
        for (let i = 0; i < SAVABLE_HELPERS.length; i++) {
            if (document.getElementById(SAVABLE_HELPERS[i]) && document.getElementById(SAVABLE_HELPERS[i]).checked) {
                checkedHelpers.push(SAVABLE_HELPERS[i])
            }
        }
        return checkedHelpers
    }

    document.getElementById('slider').addEventListener("change", saveCustomSaves);
    for (let i = 0; i < SAVABLE_HELPERS.length; i++) {
        document.getElementById(SAVABLE_HELPERS[i]).addEventListener("change", saveCustomSaves);
    }


    ///////////////
    //  helpers  //
    ///////////////
    function btnActive(elemId) {
        return ! document.getElementById(elemId).disabled
    }
    function projectVisible(elemId) {
        var e = document.getElementById(elemId);
        if (e) {
            var sv = e.style.visibility
            return sv == "visible";
        } else {
            return false
        }
    }
    function getProject(elemId) {
        var variableName = elemId.replace("Button","")
        return unsafeWindow[variableName]
    }
    function setWorkThink(value) {
        document.getElementById("slider").value = value
    }
    function divVisible(elemId) {
        return document.getElementById(elemId).style.display != 'none'
    }
    // function randInt(min, max) {
    //     return Math.floor(Math.random() * (max - min + 1)) + min;
    // }

    function optionTicked(elemId) {
        return document.getElementById(elemId).checked
    }

    function getText(elemId) {
        return document.getElementById(elemId).textContent.replace(/[,a-z ]/g,'')
    }

    function getNumber(elemId) {
        return valueOf(document.getElementById(elemId).textContent.replace(/,/g,''))
    }

    function valueOf(strValue) {
        var num = strValue.replace(/[a-z ]/g,'')
        var multiplier = strValue.replace(/[0-9. ]/g,'')
        return num * valueOfWord(multiplier)
    }

    function valueOfWord(numberName) {
        if (numberName == '') return 1

        for (const [key, value] of Object.entries(MULTIPLIERS)) {
            if (numberName == value) return key;
        }
        console.log("############## Found a number I don't know: " + numberName);
    }

    function getStage() {
        if (divVisible('businessDiv')) {
           return 1
        } else if (divVisible("spaceDiv")) {
            return 3
        } else {
            return 2
        }

    }

    function getDefaultSaveName() {
        return "u" + getText('prestigeUcounter') + "/s" + getText('prestigeScounter') + " - stage " + getStage()
    }

    const MULTIPLIERS = {
        1000 : "thousand",
        1000000 : "million",
        1000000000 : "billion",
        1000000000000 : "trillion",
        1000000000000000 : "quadrillion",
        1000000000000000000 : "quintillion",
        1000000000000000000000 : "sextillion",
        1000000000000000000000000 : "septillion",
        1000000000000000000000000000 : "octillion",
        1000000000000000000000000000000 : "nonillion",
        1000000000000000000000000000000000 : "decillion",
        1000000000000000000000000000000000000: "undecillion",
        1000000000000000000000000000000000000000: "duodecillion",
        1000000000000000000000000000000000000000000: "tredecillion",
        1000000000000000000000000000000000000000000000: "quattuordecillion",
        1000000000000000000000000000000000000000000000000: "quindecillion",
        1000000000000000000000000000000000000000000000000000: "sexdecillion",
        1000000000000000000000000000000000000000000000000000000: "septendecillion",
    }

    function getSaveSlotSelectorHtml(saveSlots) {
        var selectedSaveSlot = localStorage.getItem("helperCurrentSaveSlot");
        if (selectedSaveSlot === null) selectedSaveSlot = UNUSED_SAVE_SLOT_NAME;

        var html = "<select id='saveSlotSelector'>";

        html += getSaveSlotSelectorOptionsHtml(saveSlots, selectedSaveSlot)
        html += "</select>";
        return html;
    }


    function getSaveSlotSelectorOptionsHtml(saveSlots, selectedSaveSlot) {
        var html = "";
        var selected = "";
        var saveSlotsKeys = Object.keys(saveSlots);
        saveSlotsKeys.sort()

        for (var i = 0 ; i < saveSlotsKeys.length ; i++) {
            const slotName = saveSlotsKeys[i];
            selected = "";
            if (selectedSaveSlot === slotName) selected = " selected";
            html += "<option value='" + slotName + "'" + selected + ">" + slotName + "</option>";
        }

        if (selectedSaveSlot === UNUSED_SAVE_SLOT_NAME) selected = " selected"
        else selected = "";

        html += "<option value='" + UNUSED_SAVE_SLOT_NAME+ "'" + selected + ">empty slot</option>";

        return html
    }


    function getSelectedSlotName() {
        return document.getElementById('saveSlotSelector').value;
    }

    function loadSavedGame() {
        var slotName = getSelectedSlotName();
        if (slotName == UNUSED_SAVE_SLOT_NAME) {
            alert("Invalid slot selection.\nCan't load the empty slot.");
        } else {
            var savedGame = saveSlots[slotName];
            console.log(savedGame);

            console.log("Loading slot : " + slotName);
            localStorage.setItem('saveGame', savedGame.saveGame)
            localStorage.setItem('savePrestige', savedGame.savePrestige)
            localStorage.setItem('saveProjectsActive', savedGame.saveProjectsActive)
            localStorage.setItem('saveProjectsFlags', savedGame.saveProjectsFlags)
            localStorage.setItem('saveProjectsUses', savedGame.saveProjectsUses)
            localStorage.setItem('saveStratsActive', savedGame.saveStratsActive)
            localStorage.setItem("helperSaveSlider", savedGame.helperSaveSlider)
            localStorage.setItem("helperCurrentSaveSlot", slotName)
            localStorage.setItem("helperActiveHelpers", savedGame.helperActiveHelpers)
            window.location.reload();
        }
    }
    document.getElementById('btnGameLoader').addEventListener("click", loadSavedGame);

    function saveGame() {
        var slotName = getSelectedSlotName();

        var savedGame = {
            'saveGame':localStorage.getItem("saveGame"),
            'savePrestige':localStorage.getItem("savePrestige"),
            'saveProjectsActive':localStorage.getItem("saveProjectsActive"),
            'saveProjectsFlags':localStorage.getItem("saveProjectsFlags"),
            'saveProjectsUses':localStorage.getItem("saveProjectsUses"),
            'saveStratsActive':localStorage.getItem("saveStratsActive"),
            'helperSaveSlider':localStorage.getItem("helperSaveSlider"),
            'helperActiveHelpers':localStorage.getItem("helperActiveHelpers")
        };
        while (slotName == UNUSED_SAVE_SLOT_NAME) {
            var defaultName = getDefaultSaveName() + " - " + new Date().toISOString();
            slotName = prompt("Enter slot name", defaultName);
        }
        if (slotName) {
            if (!saveSlots.hasOwnProperty(slotName) || confirm("Update saved game in slot '" + slotName + "'?")) {
                console.log("saving to " + slotName);
                saveSlots[slotName] = savedGame;
                localStorage.setItem("saveSlots", JSON.stringify(saveSlots));
                localStorage.setItem("helperCurrentSaveSlot", slotName)
                updateSaverDiv();
            }
        } else {
            console.log("did not save");
        }
    }
    document.getElementById('btnGameSaver').addEventListener("click", saveGame);

    function clearSavedGame() {
        var slotName = getSelectedSlotName();

        if (slotName == UNUSED_SAVE_SLOT_NAME) {
            alert("Invalid slot selection.\nCan't clear the empty slot.");
        } else {
            if (confirm("Clear the saved game slot '" + slotName + "'?")) {
                console.log("clearing saved game:" + slotName);
                delete saveSlots[slotName];
                localStorage.setItem("saveSlots", JSON.stringify(saveSlots));
                updateSaverDiv();
            } else {
                console.log("did not clear.");
            }
        }
    }
    document.getElementById('btnGameSaveClearer').addEventListener("click", clearSavedGame);


    // General
    //////////////////////
    //  autobuy memory  //
    //////////////////////
    window.setInterval(function() {
        if (optionTicked("autoAddMem"))
            if (btnActive('btnAddMem'))
                addMem();
    }, 50);


    //////////////////////////
    //  autobuy processors  //
    //////////////////////////
    window.setInterval(function() {
        if (optionTicked("autoAddProcs"))
            if (btnActive('btnAddProc'))
                addProc();
    }, 50);


    ///////////////////////////////////
    //  automatic quantum computing  //
    ///////////////////////////////////
    function compute(clicks) {
        for (var j=0; j<clicks; j++) {
            qComp()
        }
    }

    function qComp_predict(){
        var q = 0;

        for (var i = 0; i<qChips.length; i++){
            q = q+qChips[i].value;
        }
        return Math.ceil(q*360)
    }

    window.setInterval(function() {
        if (optionTicked("autoQuantum")) {
            if (qComp_predict() > 0) {
                compute(100);
            }
        }
    }, 100);


    ///////////////////////////
    //  autorun tournaments  //
    ///////////////////////////
    window.setInterval(function() {
        if (optionTicked("autoTourney")) {
            if (divVisible('tournamentManagement') && btnActive('btnNewTournament'))
                newTourney()
            if (btnActive('btnRunTournament')) {
                var stratPicker = document.getElementById('stratPicker')
                if (stratPicker.options.length >= 4) {
                    stratPicker.value = 3 //GREEDY
                }
                runTourney()
            }
        }
    }, 1000);


    ///////////////////////////////////
    //  activate available projects  //
    ///////////////////////////////////


    function activateAvailableProjects() {
        for (var i = 0 ; i <= IMMEDIATE_ACTION_PROJECTS.length ; i++) {
            var projectButtonId = IMMEDIATE_ACTION_PROJECTS[i];

            if (projectVisible(projectButtonId) && btnActive(projectButtonId)) {
                if (projectButtonId == 'projectButton133' && (projectVisible('projectButton132') || projectVisible('projectButton121')))
                    continue // skip 133 (threnodies) if 132 (monument to driftwar fallen) or 121 (Name the battles) is visible... save up for 132 or 121 instead.
                if (getStage() ==3) {
                    se.eventLogger.logProject(getProject(projectButtonId))
                } else if (getStage() == 2) {
                    dr.eventLogger.logProject(getProject(projectButtonId))
                } else {
                    console.log('...activating ' + projectButtonId + ' (' + getProject(projectButtonId).title + ')')
                }
                document.getElementById(projectButtonId).click();
            }
        }
    }

    window.setInterval(function() {
        if (optionTicked('activateAvailableProjects')) {
            activateAvailableProjects
        }
    },2000);

    ///////////////////////////
    // Loggers               //
    ///////////////////////////

    class ConsoleJsonLogger {
        constructor (filterFunction) {
            this.filterFunction = filterFunction
        }
        log(obj) {
            if (! this.filterFunction || this.filterFunction() ) {
                console.log(JSON.stringify(obj))
            }
        }
    }
            
    class ElasticLogger {
        constructor() {
            this.executionStart = new Date().valueOf()
            this.index = "paperclip-log"
            this.elasticUrl = "http://192.168.1.7:32775/"
            //this.idx = 0;
        }

        log(obj) {
            var body = Object.assign({
                timestamp: new Date().valueOf(),
                executionStart: this.executionStart
            }, obj)
            // console.log(JSON.stringify(body))
            GM_xmlhttpRequest({
                method: "POST",
                url: this.elasticUrl + this.index + "/_doc/",
                headers: {  "Content-Type": "application/json"},
                data: JSON.stringify(body),
                onload: function(response) {
                    // console.log('success', response)
                },
                onerror: function(r) {
                    console.log('error POSTing data', r)
                }
            });
        }
    }

    class JsonStatusLogger {
        constructor(eventName, terms) {
            this.start = new Date().valueOf()
            this.firstLog = true
            this.logTargets = []
            this.eventName = eventName
            this.terms = terms
        }

        getTimestamp() {
            return (new Date().valueOf()-this.start)/1000
        }

        logHeader() {
            this.logWithElapsedTime({
                event:"logStart",
                scriptName: GM_info.script.name,
                scriptVersion: GM_info.script.version
            })
        }
        
        logWithElapsedTime(object) {
            var ts = {
                elapsedTime: this.getTimestamp()
            }
            Object.assign(ts, object)
            this.log(ts)
        }

        log(obj) {
            this.logTargets.forEach(function(logger) {
                logger.log(obj)
            })
        }

        resolve(obj, terms) {
            if (terms.length == 1) {
                return obj[terms[0]]
            } else {
                var t = terms.shift()
                return this.resolve(obj[t], terms)
            }
        }

        logStatus(obj) {
            if (this.firstLog) {
                this.firstLog = false
                this.logHeader()
            }

            var output = {event:this.eventName}

            this.terms.forEach(function(term) {
                output[term] = this.resolve(obj, term.split("."))
            }.bind(this))

            this.logWithElapsedTime(output)
        }
    }

    class JsonEventLogger {
        constructor() {
            this.start = new Date().valueOf()
            this.logTargets = []
        }

        getTimestamp() {
            return (new Date().valueOf()-this.start)/1000
        }

        logWithElapsedTime(object) {
            var ts = {
                elapsedTime: this.getTimestamp()
            }
            Object.assign(ts, object)
            this.log(ts)
        }

        log(obj) {
            this.logTargets.forEach(function(logger) {
                logger.log(obj)
            })
        }

        logEvent(event) {
            this.logWithElapsedTime({ event: event.trim()})
        }

        logEventOnce(event) {
            if (typeof this._logOnceMemory == 'undefined') {
                this._logOnceMemory = {}
            }
            if (typeof this._logOnceMemory[event] == 'undefined') {
                this.logWithElapsedTime({ event: event.trim()})
                this._logOnceMemory[event] = event;
            }
        }

        logProject(project) {
            this.logWithElapsedTime({
                event: "activating project",
                projectId: project.id,
                projectTitle: project.title.trim()
            })
        }
    }



    // Phase 1
    ///////////////////////////
    //  automake paperclips  //
    ///////////////////////////
    window.setInterval(function() {
        if (
            optionTicked("autoMakePaperclips")
            &&
            (
                ! divVisible('wireBuyerDiv')
                ||
                (
                    divVisible('wireBuyerDiv')
                    &&
                    getText('wireBuyerStatus') != 'ON'
                )
            )
            &&
            btnActive('btnMakePaperclip')) {
            clipClick(10000000);
        }
    }, 100);

    ////////////////////
    //  autobuy wire  //
    ////////////////////
    window.setInterval(function() {
        if (optionTicked("autoBuyWire")) {
            if (getText('wire') == 0) {
                buyWire()
            }
        }
    }, 100);



    // Phase 2
    //////////////////////////
    //  auto-plays stage 2  //
    //////////////////////////
    const SOLAR_BUTTONS = {
        1: 'btnMakeFarm',
        10: 'btnFarmx10',
        100:'btnFarmx100'
    }

    const STORAGE_BUTTONS = {
        1: 'btnMakeBattery',
        10: 'btnBatteryx10',
        100:'btnBatteryx100'
    }

    const WIRE_DRONE_BUTTONS = {
        1: 'btnMakeWireDrone',
        10: 'btnWireDronex10',
        100:'btnWireDronex100',
        1000:'btnWireDronex1000'
    }

    const HARVESTER_BUTTONS = {
        1: 'btnMakeHarvester',
        10: 'btnHarvesterx10',
        100:'btnHarvesterx100',
        1000:'btnHarvesterx1000'
    }

    const MAX_SOLAR_LOAD_WHEN_CHARGING = .78
    const MAX_SOLAR_LOAD_WHEN_FULLY_CHARGED = .95
    const MAX_REQUIRED_SOLAR_STORAGE = 10000000

    const DRONE_COUNT_THRESHHOLD_X10 = 550
    const DRONE_COUNT_THRESHHOLD_X100 = 1200
    const DRONE_COUNT_THRESHHOLD_X1000 = 10000

    class DroneWorld {
        constructor () {
            this.consumption = 0;
            this.production = 0;
            this.storedPower = 0;
            this.maxStorage = 0;

            this.availableMatter = 0;
            this.acquiredMatter = 0;
            this.acquiredWire = 0;

            this.multiplier = 1;

            this.harvesterDrones = 0;
            this.wireDrones = 0;

            this.workThink = 0;
        }

        process() {
            if (getStage() == 2 && optionTicked("autoPlayPhase2")) {
                this.updateValues();
                if (divVisible("entertainButtonDiv") && btnActive('btnEntertainSwarm')) entertainSwarm()
                if (divVisible("synchButtonDiv") && btnActive('btnSynchSwarm')) synchSwarm()
                if (this.availableMatter > 0 || this.acquiredMatter > 0 || this.acquiredWire > 0) {
                    this.takeAction();
                    this.statusLogger.logStatus(this)
                } else {
                    this.eventLogger.logEventOnce("done with stage 2")
                    this.think()
                }
            }
        }

        /* ************************************************************ */
        /* ************************************************************ */

        updateValues() {
            this.consumption = parseFloat(getText('powerConsumptionRate'))
            this.production = parseFloat(getText('powerProductionRate'))
            this.storedPower = parseFloat(getText('storedPower'))
            this.maxStorage = parseFloat(getText('maxStorage'))

            this.acquiredMatter = parseFloat(getText('acquiredMatterDisplay'));
            this.availableMatter = parseFloat(getText('availableMatterDisplay'));

            this.acquiredWire = parseFloat(getText('nanoWire'));

            this.harvesterDrones = parseFloat(getText('harvesterLevelDisplay'));
            this.wireDrones = parseFloat(getText('wireDroneLevelDisplay'));

            this.workThink = document.getElementById("slider").value;

            this.setMultiplier();
        }

        setMultiplier() {
            this.multiplier = 1;
            this.droneMultiplier = 1;

            if (this.harvesterDrones + this.wireDrones > DRONE_COUNT_THRESHHOLD_X10) {
                this.multiplier = 10;
                this.droneMultiplier = 10;
            }
            if (this.harvesterDrones + this.wireDrones > DRONE_COUNT_THRESHHOLD_X100) {
                this.multiplier = 100;
                this.droneMultiplier = 100;
            }
            if (this.harvesterDrones + this.wireDrones > DRONE_COUNT_THRESHHOLD_X1000) {
                this.multiplier = 100;
                this.droneMultiplier = 1000;
            }
        }

        /* ************************************************************ */
        /* ************************************************************ */

        work() {
            setWorkThink(10)
            this.eventLogger.logEventOnce('shifting attention to working')
        }

        think() {
            setWorkThink(190) // 95% think
            this.eventLogger.logEventOnce('shifting attention to thinking')
        }

        takeAction() {
            //console.log("projectButton102 : " + projectVisible("projectButton102"));
            if (projectVisible("projectButton102")) {
                // Self-correcting Supply Chain - (1 sextillion clips) - Each factory added to the network increases every factory's output 1,000x
                // Best to stop buying, and wait for enough chips to buy this.
                if (btnActive("projectButton102")) {
                    document.getElementById("projectButton102").click();
                    this.think()
                } else {
                    this.eventLogger.logEventOnce('...waiting for projectButton102 (Self-Correcting Supply Chain) to be clickable')
                    this.work()
                }
            } else {
                activateAvailableProjects()
                this.think()
                if (this.powerOk()) {
                    //console.log("acquiredMatter:" + this.acquiredMatter)
                    //console.log("acquiredWire:" + this.acquiredWire)
                    if (this.acquiredWire > 0) {
                        this.buyFactory()
                    } else if (this.acquiredMatter > 0) {
                        this.buyWireDrone()
                    } else if (this.acquiredMatter == 0 && this.availableMatter > 0) {
                        this.buyHarvesterDrone()
                    } else {
                        // do nothing.  See which way it goes.
                    }
                } else {
                    this.buySolar()

                    // Special condition at start of level - have to bootstrap even with insufficient solar production.
                    if (this.harvesterDrones == 0) this.buyHarvesterDrone()
                    if (this.wireDrones == 0) this.buyWireDrone()
                }
                if (this.needMoreStorage()) {
                    this.buyStorage()
                }
            }
        }

        powerOk() {
            var load = this.consumption / this.production;
            if (this.needSurplusPower())
                return load < MAX_SOLAR_LOAD_WHEN_CHARGING;
            else
                return load < MAX_SOLAR_LOAD_WHEN_FULLY_CHARGED;
        }

        needSurplusPower() {
            return this.storedPower < this.maxStorage;
        }

        needMoreStorage() {
            return this.storedPower == this.maxStorage && this.maxStorage < MAX_REQUIRED_SOLAR_STORAGE;
        }

        buyFactory() {
            if (btnActive('btnMakeFactory')) {
                this.eventLogger.logEvent("buying factory")
                makeFactory()
            } else {
                this.eventLogger.logEvent('...waiting to buy factory')
                this.work()
            }
        }

        buyWireDrone() {
            if (btnActive(WIRE_DRONE_BUTTONS[this.droneMultiplier])) {
                this.eventLogger.logEvent("buying " + this.droneMultiplier +" wire drone(s)")
                makeWireDrone(this.droneMultiplier)
            } else {
                this.eventLogger.logEvent('...waiting to buy ' + this.droneMultiplier + ' wire drone(s)')
                this.work()
            }
        }

        buyHarvesterDrone() {
            if (btnActive(HARVESTER_BUTTONS[this.droneMultiplier])) {
                this.eventLogger.logEvent("buying " + this.droneMultiplier +" harvester(s)")
                makeHarvester(this.droneMultiplier)
            } else {
                this.eventLogger.logEvent('...waiting to buy ' + this.droneMultiplier + ' harvester(s)')
                this.work()
            }
        }

        buySolar() {
            if (btnActive(SOLAR_BUTTONS[this.multiplier])) {
                this.eventLogger.logEvent("buying " + this.multiplier +" solar farm(s)")
                makeFarm(this.multiplier);
            } else {
                this.eventLogger.logEvent('...waiting to buy ' + this.multiplier + ' solar farm(s)')
                this.work()
            }
        }

        buyStorage() {
            if (btnActive(STORAGE_BUTTONS[this.multiplier])) {
                this.eventLogger.logEvent("buying " + this.multiplier +" battery(ies)")
                makeBattery(this.multiplier);
            } else {
                this.eventLogger.logEvent('...waiting to buy ' + this.multiplier + ' battery(ies)')
                this.work()
            }
        }
    }
    var dw = new DroneWorld();

    dw.statusLogger = new JsonStatusLogger("Stage 2 Status Update", ['consumption','production','storedPower','maxStorage','availableMatter','acquiredMatter','acquiredWire','multiplier','harvesterDrones','wireDrones','workThink'])
    dw.statusLogger.logTargets.push(new  ConsoleJsonLogger(function() { return optionTicked('logDWStats') } ))
    dw.statusLogger.logTargets.push(new  ElasticLogger())

    dw.eventLogger = new JsonEventLogger()
    dw.eventLogger.logTargets.push(new  ConsoleJsonLogger())
    dw.eventLogger.logTargets.push(new  ElasticLogger())

    window.setInterval(dw.process.bind(dw), 500);

    // Phase 3
    /////////////////////////
    //  Auto-play Stage 3  //
    /////////////////////////
    const GROWTH_MODE_NON_COMBAT = {
        spd:0,
        exp:0,
        srp:"13w",  // 12w
        hrm:5,      // 8w
        fac:0,
        hvs:0,
        wir:0,
        cbt:0
    }

    const GROWTH_MODE_COMBAT = {
        spd:0,
        exp:0,
        srp:"10w",
        hrm:"5w",
        fac:0,
        hvs:0,
        wir:0,
        cbt:"5w"
    }

    const GROWTH_MODE_COMBAT_OODA = {
        spd:"2w",
        exp:0,
        srp:"10w",
        hrm:"5w",
        fac:0,
        hvs:0,
        wir:0,
        cbt:"3w"
    }

    const EXPLORE_BUILD_MODE_NON_COMBAT = {
        spd:3,      // 2
        exp:3,      // 2
        srp:"10w",  
        hrm:"8w",   
        fac:1,      
        hvs:2,
        wir:3,
        cbt:0
    }

    const EXPLORE_BUILD_MODE_COMBAT = {
        spd:3,      // 2
        exp:3,      // 2
        srp:"10w",
        hrm:"4w",
        fac:1,
        hvs:2,
        wir:3,
        cbt:"4w"
    }

    const HYPER_EXPLORE_MODE_COMBAT = {
        spd:"3w",
        exp:"3w",
        srp:4,
        hrm:6,
        fac:0,
        hvs:0,
        wir:0,
        cbt:6
    }



    class SpaceExploration {
        constructor () {
            this.doneWithStage = false
            this.logger = null
            this.lastCycleTime = 0;
            this.CYCLE_DELAY_MS = 12000
            this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PUE_THRESHOLD = .1
            this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PROBE_THRESHOLD = 4 * 10 ** 32
            this.PROBES_PER_LAUNCH = 2000
            this.AUTOLAUNCH_PROBES_IF_TOTAL_COUNT_LESS_THAN = 1500000
            this.SACRIFICE_PROBES_PRIOR_TO_COMBAT = 1000000000
            this.logger = null

            this.clipsPerSecond = 0
            this.unusedClips = 0

            this.matterAvailable = 0
            this.matterAcquired = 0
            this.wire = 0
            this.harvesterDrones = 0
            this.wireDrones = 0
            this.percentUniverseExplored = 0
            this.probesLaunched = 0
            this.probesBorn = 0
            this.probesLostToHazards = 0
            this.probesLostToValueDrift = 0
            this.probesLostInCombat = 0
            this.probesTotal = 0
            this.driftersKilled = 0
            this.drifters = 0

            this.operations = 0
            this.maxOps = 0
            this.creativity = 0
            this.trust = 0
            this.trustUsed = 0
            this.trustMax = 0
            this.probeSpeed = 0
            this.probeExploration = 0
            this.probeSelfReplication = 0
            this.probeHazardRemediation = 0
            this.probeFactoryProduction = 0
            this.probeHarvesterDroneProduction = 0
            this.probeWireDroneProduction = 0
            this.probeCombat = 0

            this.jumpstartLevel = 0

            this.probeTrustFunctions = {
                spd: {incr: function() {raiseProbeSpeed()} , decr: function() {lowerProbeSpeed()} },
                exp: {incr: function() {raiseProbeNav()} , decr: function() {lowerProbeNav()} },
                srp: {incr: function() {raiseProbeRep()} , decr: function() {lowerProbeRep()} },
                hrm: {incr: function() {raiseProbeHaz()} , decr: function() {lowerProbeHaz()} },
                fac: {incr: function() {raiseProbeFac()} , decr: function() {lowerProbeFac()} },
                hvs: {incr: function() {raiseProbeHarv()} , decr: function() {lowerProbeHarv()} },
                wir: {incr: function() {raiseProbeWire()} , decr: function() {lowerProbeWire()} },
                cbt: {incr: function() {raiseProbeCombat()} , decr: function() {lowerProbeCombat()} }
            }
        }

        process() {
            if (divVisible('spaceDiv')) {
                this.updateValues();

                if (optionTicked("autoPlayPhase3")) {

                    this.autoLaunchProbes();
                    this.chooseAndSetMode();
                    this.setProbeTrustConfigurationModel()
                    this.setProbeTrustConfiguration()

                    if (this.unusedClips > 1000000000000000000 /* 1 Quintillion - way more than enough to buy probes @ 1 Quad */ ) {
                        setWorkThink(199) // 99.5% think
                    } else {
                        setWorkThink(0)   // Working full time
                    }

                    if (!this.doneWithStage) {
                        if (btnActive("btnIncreaseProbeTrust")) increaseProbeTrust()
                        if (btnActive("btnIncreaseMaxTrust") && this.trust == this.trustMax) increaseMaxTrust()
                        if (divVisible("entertainButtonDiv") && btnActive('btnEntertainSwarm')) entertainSwarm()
                        if (divVisible("synchButtonDiv") && btnActive('btnSynchSwarm')) synchSwarm()

                        this.statusLogger.logStatus(this)

                        if (this.percentUniverseExplored == 100 && this.matterAvailable == 0 && this.matterAcquired == 0 && this.wire == 0) {
                            this.doneWithStage = true
                            this.eventLogger.logEvent("done with stage 3")
                        }
                    }
                }
            }
        }

        autoLaunchProbes() {
            if (this.probesTotal < this.AUTOLAUNCH_PROBES_IF_TOTAL_COUNT_LESS_THAN && btnActive('btnMakeProbe')) {
                for (var i=0; i<this.PROBES_PER_LAUNCH; i++) {
                    makeProbe();
                }
            }
        }

        chooseAndSetMode() {
            if (this.percentUniverseExplored > this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PUE_THRESHOLD && this.probesTotal > this.HYPER_EXPLORE_MODE_COMBAT_CUTOVER_PROBE_THRESHOLD) {
                this.mode = HYPER_EXPLORE_MODE_COMBAT
                this.eventLogger.logEventOnce("Force HYPER_EXPLORE_MODE_COMBAT selection!")
            } else {
                if (this.lastCycleTime + this.CYCLE_DELAY_MS < this.now) {
                    this.lastCycleTime = this.now;

                    if (!optionTicked('ignoreCombat') && this.isCombatEnabled() && this.probesLostInCombat > this.SACRIFICE_PROBES_PRIOR_TO_COMBAT) {
                        this.mode = EXPLORE_BUILD_MODE_COMBAT;
                    } else {
                        this.mode = EXPLORE_BUILD_MODE_NON_COMBAT;
                    }
                } else {
                    if (!optionTicked('ignoreCombat') && this.isCombatEnabled() && this.probesLostInCombat > this.SACRIFICE_PROBES_PRIOR_TO_COMBAT) {
                        if (project120.flag) {
                            // we have OODA Loop
                            this.mode = GROWTH_MODE_COMBAT_OODA
                        } else {
                            this.mode = GROWTH_MODE_COMBAT
                        }
                    } else {
                        this.mode = GROWTH_MODE_NON_COMBAT;
                    }
                }
            }
        }

        setProbeTrustConfigurationModel() {
            var probeTrustConfiguration = {}
            var rawProbeTrustConfiguration = {}
            var sumOfWeights = this.getSumOfParameterWeights(this.mode)
            var sumOfNumerics = this.getSumOfParameterNumerics(this.mode)
            var maxWeighted = this.getMaxWeightedParameter(this.mode)

            //console.log('sum W :' + sumOfWeights)
            //console.log('sum N :' + sumOfNumerics)
            //console.log('mx key:' + maxWeighted)

            for (const [key, value] of Object.entries(this.mode)) {
                var v = null;
                if (typeof value != 'string') {
                    v = value
                    rawProbeTrustConfiguration[key] = v
                } else {
                    v = getConfigWeight(value)/sumOfWeights * (this.trust - sumOfNumerics)
                    rawProbeTrustConfiguration[key] = v
                    if (key == maxWeighted) {
                        v = Math.ceil(v)
                    } else {
                        v = Math.floor(v)
                    }
                    v = Math.round(v)
                }
                probeTrustConfiguration[key] = v
            }

            while (this.getSumOfParameterNumerics(probeTrustConfiguration) < this.trust) {
                probeTrustConfiguration[maxWeighted]++
            }

            //console.log(this.mode)
            //console.log(rawProbeTrustConfiguration)
            //console.log(probeTrustConfiguration)
            this.probeTrustConfigurationModel = probeTrustConfiguration;
        }


        setProbeTrustConfiguration() {
            var ptf = this.probeTrustFunctions;
            
            this.applyProbeTrustLogic(
                function(key, standard, currentProbeTrustConfiguration) {
                    var ct = currentProbeTrustConfiguration[key] - standard
                    while (ct > 0) {
                        ptf[key].decr();
                        currentProbeTrustConfiguration[key]--;
                        ct--;
                    }
                }, this.probeTrustConfigurationModel)

            buttonUpdate();
            
            this.applyProbeTrustLogic(
                function(key, standard, currentProbeTrustConfiguration) {
                    var ct = currentProbeTrustConfiguration[key] - standard
                    while (ct < 0) {
                        ptf[key].incr();
                        currentProbeTrustConfiguration[key]++;
                        ct++;
                    }
                }, this.probeTrustConfigurationModel)
        }

        applyProbeTrustLogic(executor, probeTrustConfigurationModel) {
            for (const [key, value] of Object.entries(probeTrustConfigurationModel)) {
                var i = 0;
                while(executor(key, value, this.probeTrustConfiguration) && i < 100) {
                    i++
                }
            }
        }

        getSumOfParameterWeights(mode) {
            var sum = 0;

            for (const [key, value] of Object.entries(mode)) {
                sum += getConfigWeight(value)
            }
            return sum
        }

        getSumOfParameterNumerics(mode) {
            var sum = 0;

            for (const [key, value] of Object.entries(mode)) {
                if (typeof value == typeof 0) {
                    sum += value
                }
            }
            return sum
        }

        getMaxWeightedParameter(mode) {
            var mx = 0;
            var keyMax = null;
            for (const [key, value] of Object.entries(mode)) {
                var v = getConfigWeight(value)
                if (v >= mx) {
                    mx = v
                    keyMax = key
                }
            }
            return keyMax
        }

        updateValues() {
            this.now = new Date().valueOf();
            this.clips = getNumber('clips')
            this.clipsPerSecond = getNumber('clipmakerRate2')
            this.unusedClips = getNumber('unusedClipsDisplay')
            this.matterAvailable = getNumber('availableMatterDisplay')
            this.matterAcquired = getNumber('acquiredMatterDisplay')
            this.wire = getNumber('nanoWire')
            this.harvesterDrones = getNumber('harvesterLevelSpace')
            this.wireDrones = getNumber('wireDroneLevelSpace')

            this.percentUniverseExplored = getNumber('colonizedDisplay')
            this.probesLaunched = getNumber('probesLaunchedDisplay')
            this.probesBorn = getNumber('probesBornDisplay')
            this.probesLostToHazards = getNumber('probesLostHazardsDisplay')
            this.probesLostToValueDrift = getNumber('probesLostDriftDisplay')
            this.probesLostInCombat = getNumber('probesLostCombatDisplay')
            this.probesTotal = getNumber('probesTotalDisplay')
            this.driftersKilled = getNumber('driftersKilled')
            this.drifters = getNumber('drifterCount')
            this.operations = getNumber("operations")
            this.maxOps = getNumber("maxOps")
            this.creativity = getNumber("creativity")
            this.trust = parseFloat(getNumber('probeTrustDisplay'))
            this.trustUsed = parseFloat(getNumber('probeTrustUsedDisplay'))
            this.trustMax = parseFloat(getNumber('maxTrustDisplay'))

            this.probeTrustConfiguration = {
                spd:getNumber('probeSpeedDisplay'),
                exp:getNumber('probeNavDisplay'),
                srp:getNumber('probeRepDisplay'),
                hrm:getNumber('probeHazDisplay'),
                fac:getNumber('probeFacDisplay'),
                hvs:getNumber('probeHarvDisplay'),
                wir:getNumber('probeWireDisplay'),
                cbt:getNumber('probeCombatDisplay')
            }

            this.probeSpeed = getNumber('probeSpeedDisplay')
            this.probeExploration = getNumber('probeNavDisplay')
            this.probeSelfReplication = getNumber('probeRepDisplay')
            this.probeHazardRemediation = getNumber('probeHazDisplay')
            this.probeFactoryProduction = getNumber('probeFacDisplay')
            this.probeHarvesterDroneProduction = getNumber('probeHarvDisplay')
            this.probeWireDroneProduction = getNumber('probeWireDisplay')
            this.probeCombat = getNumber('probeCombatDisplay')

            if (this.percentUniverseExplored > 0 && typeof this.universeMoved == 'undefined') {
                this.eventLogger.logEvent('universe just moved')
                this.universeMoved = true
            }
        }

        isCombatEnabled() {
            return divVisible('combatButtonDiv')
        }
    }

    function getConfigWeight(value) {
        var pattern = "([0-9.]+)w"
        if (typeof value == 'string') {
            if (value.match(pattern)) {
                return parseFloat(value.match(pattern)[1])
            }
            console.log("Error extracting weight from value '" + value + "'")
        }
        return 0
    }

    var se = new SpaceExploration();

    se.statusLogger = new JsonStatusLogger('Stage 3 Status Update', ['clips','unusedClips','matterAvailable','matterAcquired','wire','percentUniverseExplored','probesLaunched','probesBorn','probesTotal','harvesterDrones','wireDrones','trust','probeTrustConfigurationModel.srp','operations','creativity'])
    se.statusLogger.logTargets.push(new  ConsoleJsonLogger(function() { return optionTicked('logSEStats') } ))
    se.statusLogger.logTargets.push(new  ElasticLogger())

    se.eventLogger = new JsonEventLogger()
    se.eventLogger.logTargets.push(new  ConsoleJsonLogger())
    se.eventLogger.logTargets.push(new  ElasticLogger())


    window.setInterval(se.process.bind(se), 300);

    ////////////////////////
    //  autolaunch probe  //
    ////////////////////////
    window.setInterval(function() {
        if (
            divVisible('spaceDiv') != 'none'
            &&
            optionTicked("autoLaunchProbe")
            &&
            ! optionTicked("autoPlayPhase3")
            &&
            btnActive('btnMakeProbe')) {
            for (var i=0; i<1000; i++) {
                makeProbe();
            }
        }
    }, 100);

})();